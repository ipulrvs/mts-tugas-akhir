import Head from 'next/head'

function IndexPage() {
  return (
    <div>
      <Head>
        <title>Aplikasi</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

        <link rel="icon" type="image/png" sizes="16x16" href="/zenix/images/favicon.png" />
        <link rel="stylesheet" href="/zenix/vendor/chartist/css/chartist.min.css" />
        <link href="/zenix/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="/zenix/vendor/owl-carousel/owl.carousel.css" rel="stylesheet" />
        <link href="/zenix/css/style.css" rel="stylesheet"></link>

        <script src="/zenix/vendor/global/global.min.js"></script>
        <script src="/zenix/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="/zenix/vendor/chart.js/Chart.bundle.min.js"></script>
        <script src="/zenix/vendor/peity/jquery.peity.min.js"></script>
        <script src="/zenix/vendor/apexchart/apexchart.js"></script>
        {/* <script src="/zenix/js/dashboard/dashboard-1.js"></script> */}
        <script src="/zenix/vendor/owl-carousel/owl.carousel.js"></script>
        <script src="/zenix/js/custom.js"></script>
        <script src="/zenix/js/deznav-init.js"></script>

        <link href="/summernote/summernote.min.css" rel="stylesheet" />
        <script src="/summernote/summernote.min.js"></script>
      </Head>
    </div>
  )
}

export default IndexPage
