import styles from '../styles/Home.module.css'
import constant from '../components/constant'
import http from '../components/http.service'
import React from 'react'
import Head from '../components/Head'

export async function getServerSideProps() {
  // Fetch data
  var data = {}
  return { props: { data } }
}

class HomeComponent extends React.Component {
  async componentDidMount(){
    try {
      var token = localStorage.getItem('mts-token')
      let res = await http.get('http://localhost:7070/profile', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })

      let setting = await http.get(`http://localhost:7070/setting/1`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      localStorage.setItem("mts-tema", JSON.stringify(setting));

      window.location.href="/app"
    } catch(e){
      window.location.href="/login"
    }
  }

  render(){
    return <div />
  }
}

function Home({ data }) {
  return (
    <>
      <Head></Head>
      <HomeComponent data={data} />
    </>
  )
}

export default Home
