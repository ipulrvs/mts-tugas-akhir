import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from '../app/navbar'
import Sidebar from '../app/sidebar-proyek'
import Swal from 'sweetalert2'
import Router from 'next/router'

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class PageComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "Master Tugas | Formulir Tugas",
            id: null,
            nama: null,
            keterangan: null,
            status_id: null,
            timeline_id: null,
            sprint_id: null,
            tipe_tugas_id: null,
            tugas_dilaporkan_oleh_id: null,
            tugas_dikerjakan_oleh_id: null,
            prioritas: null,
            proyek_id: null,
            status: [],
            tipe_tugas: [],
            timeline: [],
            sprint: [],
            tugas_dilaporkan_oleh: [],
            tugas_dikerjakan_oleh: [],
            estimasi_awal: 0,
            estimasi_dikerjakan: 0,
            point: 0,
            errorMessage: [],
            formIsError: false
        }
    }

    async initData() {
        var query = Router.query
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get(`http://localhost:7070/task/${query.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            var data = {
                prioritas: res.data.prioritas,
                nama: res.data.nama,
                keterangan: res.data.keterangan,
                proyek_id: res.data.proyek_id,
                estimasi_awal: res.data.estimasi_awal,
                estimasi_dikerjakan: res.data.estimasi_dikerjakan,
                id: res.data.id
            }
            try {
                data.status_id = res.data.status.id
            } catch (e) { }
            try {
                data.tipe_tugas_id = res.data.tipe_tugas.id
            } catch (e) { }
            try {
                data.timeline_id = res.data.timeline.id
            } catch (e) { }
            try {
                data.sprint_id = res.data.sprint.id
            } catch (e) { }
            try {
                data.tugas_dilaporkan_oleh_id = res.data.tugas_dilaporkan_oleh.id
            } catch (e) { }
            try {
                data.tugas_dikerjakan_oleh_id = res.data.tugas_dikerjakan_oleh.id
            } catch (e) { }
            this.setState(data)
            var keterangan = $('#summernote').summernote('code', res.data.keterangan);
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/kolom`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                status: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/tipe_tugas`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                tipe_tugas: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/timeline`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                timeline: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/sprint`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                sprint: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/user`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                tugas_dilaporkan_oleh: res.data.data,
                tugas_dikerjakan_oleh: res.data.data
            });
        } catch (e) { }
    }

    async componentDidMount() {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        this.initData()
        $(document).ready(function () {
            $('#summernote').summernote({
                minHeight: 350
            });
        });
    }

    async handleSave() {
        var keterangan = $('#summernote').summernote('code');
        console.log(keterangan)
        var errorMessage = ""
        if (!this.state.nama) {
            errorMessage += "nama diperlukan,"
        }
        if (!this.state.status_id) {
            errorMessage += " status diperlukan,"
        }
        if (!keterangan || keterangan.length <= 11) {
            errorMessage += " keterangan diperlukan,"
        }
        if (!this.state.tipe_tugas_id) {
            errorMessage += " tipe tugas diperlukan,"
        }
        if (errorMessage == "") {
            var fromData = {
                prioritas: this.state.prioritas,
                nama: this.state.nama,
                keterangan: keterangan,
                id: this.state.id,
                estimasi_awal: this.state.estimasi_awal,
                estimasi_dikerjakan: this.state.estimasi_awal,
                status: {
                    id: this.state.status_id
                },
                tipe_tugas: {
                    id: this.state.tipe_tugas_id
                },
                timeline: {
                    id: this.state.timeline_id
                },
                sprint: {
                    id: this.state.sprint_id
                },
                tugas_dilaporkan_oleh: {
                    id: this.state.tugas_dilaporkan_oleh_id
                },
                tugas_dikerjakan_oleh: {
                    id: this.state.tugas_dikerjakan_oleh_id
                },
                proyek_id: this.state.proyek_id ? this.state.proyek_id : JSON.parse(localStorage.getItem("mts-proyek")).id
            }
            try {
                var token = localStorage.getItem('mts-token')
                if (this.state.id) {
                    let res = await http.put(`http://localhost:7070/task/${this.state.id}`, fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                } else {
                    let res = await http.post('http://localhost:7070/task', fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                }
                Swal.fire('Sukses', 'Data berhasil disimpan', 'success')
                setTimeout(() => {
                    window.location.href = "/task-proyek"
                }, 1000)
            } catch (e) {
                Swal.fire('Gagal', `Data tidak berhasil disimpan dengan pesan error ${e.message}`, 'error')
            }
        } else {
            this.setState({
                formIsError: true,
                errorMessage: errorMessage
            })
        }
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    <Sidebar />
                    <div className="content-body">
                        <div className='container-fluid'>
                            <div className="card">
                                <div className="card-body">
                                    <div className="basic-form">
                                        <div className='mb3'>
                                            <div className="row">
                                                <div className='col-md-12'>
                                                    {this.state.formIsError &&
                                                        <div className="alert alert-danger solid alert-dismissible fade show">
                                                            <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" className="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                            {/* <strong>Error!</strong>  */}
                                                            {this.state.errorMessage}
                                                            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                                            </button>
                                                        </div>
                                                    }
                                                </div>
                                                <div className="mb-3 col-md-8">
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Judul Task dan Issue</label>
                                                        <input disabled value={this.state.nama} onChange={(e) => { this.setState({ nama: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Keterangan</label>
                                                        <div id="summernote"></div>
                                                    </div>
                                                </div>
                                                <div className="mb-3 col-md-4 md-grid row">
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Status</label>
                                                        <select disabled
                                                            value={this.state.status_id}
                                                            onChange={(e) => {
                                                                this.setState({ status_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.status &&
                                                                this.state.status.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.status)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.status.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Tipe Tugas</label>
                                                        <select disabled
                                                            value={this.state.tipe_tugas_id}
                                                            onChange={(e) => {
                                                                this.setState({ tipe_tugas_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.tipe_tugas &&
                                                                this.state.tipe_tugas.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.tipe_tugas)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.tipe_tugas.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Timeline</label>
                                                        <select disabled
                                                            value={this.state.timeline_id}
                                                            onChange={(e) => {
                                                                this.setState({ timeline_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.timeline &&
                                                                this.state.timeline.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.timeline)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.timeline.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Sprint</label>
                                                        <select disabled
                                                            value={this.state.sprint_id}
                                                            onChange={(e) => {
                                                                this.setState({ sprint_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.sprint &&
                                                                this.state.sprint.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.sprint)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.sprint.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Dikerjakan Oleh</label>
                                                        <select disabled
                                                            value={this.state.tugas_dikerjakan_oleh_id}
                                                            onChange={(e) => {
                                                                this.setState({ tugas_dikerjakan_oleh_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.tugas_dikerjakan_oleh &&
                                                                this.state.tugas_dikerjakan_oleh.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.tugas_dikerjakan_oleh)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.tugas_dikerjakan_oleh.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Dilaporkan Oleh</label>
                                                        <select disabled
                                                            value={this.state.tugas_dilaporkan_oleh_id}
                                                            onChange={(e) => {
                                                                this.setState({ tugas_dilaporkan_oleh_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.tugas_dilaporkan_oleh &&
                                                                this.state.tugas_dilaporkan_oleh.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.tugas_dilaporkan_oleh)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.tugas_dilaporkan_oleh.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Prioritas</label>
                                                        <select disabled
                                                            value={this.state.prioritas}
                                                            onChange={(e) => {
                                                                this.setState({ prioritas: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            <option value={null}>-</option>
                                                            <option value="Sangat Rendah">Sangat Rendah</option>
                                                            <option value="Rendah">Rendah</option>
                                                            <option value="Sedang">Sedang</option>
                                                            <option value="Tinggi">Tinggi</option>
                                                            <option value="Sangat Tinggi">Sangat Tinggi</option>
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Point (0 - 100)</label>
                                                        <input disabled value={this.state.point} onChange={(e) => { this.setState({ point: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" min={0} max={100} placeholder="" />
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Estimasi Awal (Jam)</label>
                                                        <input disabled value={this.state.estimasi_awal} onChange={(e) => { this.setState({ estimasi_awal: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" min={0} max={100} placeholder="" />
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Estimasi Dikerjakan (Jam)</label>
                                                        <input disabled value={this.state.estimasi_dikerjakan} onChange={(e) => { this.setState({ estimasi_dikerjakan: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" min={0} max={100} placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="/task-timeline" style={{ marginRight: '10px' }} className="btn btn-xs btn-info">Kembali</a>
                                            {/* <button className="btn btn-xs btn-success" onClick={() => { this.handleSave() }}>Simpan Data</button> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    )
}

export default Page
