import constant from "../../components/constant";
import http from "../../components/http.service";
import React from "react";
import Head from "../../components/Head";
import Navbar from "../app/navbar";
import Sidebar from "../app/sidebar";
import Swal from "sweetalert2";
import Router from "next/router";

export async function getServerSideProps() {
    // Fetch data
    var data = {};
    return { props: { data } };
}

class PageComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageTitle: "Formulir Pengguna",
            id: null,
            username: null,
            password: null,
            nama: null,
            role_id: null,
            departemen_id: null,
            jabatan_id: null,
            errorMessage: [],
            formIsError: false,
            role: [],
            jabatan: [],
            departemen: [],
            jadwal: [],
            foto: "",
        };
    }

    async initData() {
        var query = Router.query;
        var token = localStorage.getItem("mts-token");
        try {
            let res = await http.get(`http://localhost:7070/role`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            this.setState({
                role: res.data.data,
            });
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/jabatan`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            this.setState({
                jabatan: res.data.data,
            });
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/departemen`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            this.setState({
                departemen: res.data.data,
            });
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/jadwal`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            this.setState({
                jadwal: res.data.data,
            });
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/user/${query.id}`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            this.setState({
                username: res.data.username,
                password: res.data.password,
                nama: res.data.nama,
                role_id: res.data.role.id,
                departemen_id: res.data.departemen.id,
                jabatan_id: res.data.jabatan.id,
                jadwal_id: res.data.jadwal.id,
                foto: res.data.foto,
                id: res.data.id,
            });
        } catch (e) { }
    }

    async componentDidMount() {
        var token = localStorage.getItem("mts-token");
        try {
            let res = await http.get("http://localhost:7070/profile", {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login";
        }
        this.initData();
    }

    async initData() {
        var query = Router.query
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get(`http://localhost:7070/role`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            this.setState({
                role: res.data.data
            })
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/jabatan`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            this.setState({
                jabatan: res.data.data
            })
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/departemen`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            this.setState({
                departemen: res.data.data
            })
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/jadwal`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            this.setState({
                jadwal: res.data.data
            })
        } catch (e) { }
        try {
            let res = await http.get(`http://localhost:7070/user/${query.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            var data = {
                username: res.data.username,
                password: res.data.password,
                nama: res.data.nama,
                foto: res.data.foto,
                id: res.data.id
            }
            try { data.role_id = res.data.role.id } catch (e) { }
            try { data.departemen_id = res.data.departemen.id } catch (e) { }
            try { data.jabatan_id = res.data.jabatan.id } catch (e) { }
            try { data.jadwal_id = res.data.jadwal.id } catch (e) { }
            this.setState(data)
        } catch (e) { }
    }


    async handleSave() {
        var errorMessage = "";
        if (!this.state.username) {
            errorMessage += "username diperlukan,";
        }
        if (!this.state.password) {
            errorMessage += " password diperlukan,";
        }
        if (!this.state.nama) {
            errorMessage += " nama diperlukan,";
        }
        if (!this.state.role_id) {
            errorMessage += " role diperlukan,";
        }
        if (!this.state.departemen_id) {
            errorMessage += " departemen diperlukan,";
        }
        if (!this.state.jabatan_id) {
            errorMessage += " jabatan diperlukan,";
        }
        if (!this.state.jadwal_id) {
            errorMessage += " jadwal diperlukan,";
        }
        if (errorMessage == "") {
            var fromData = {
                foto: this.state.foto,
                username: this.state.username,
                password: this.state.password,
                nama: this.state.nama,
                role: {
                    id: this.state.role_id,
                },
                jabatan: {
                    id: this.state.jabatan_id,
                },
                departemen: {
                    id: this.state.departemen_id,
                },
                jadwal: {
                    id: this.state.jadwal_id,
                },
                id: this.state.id,
            };
            try {
                var token = localStorage.getItem("mts-token");
                if (this.state.id) {
                    let res = await http.put(
                        `http://localhost:7070/user/${this.state.id}`,
                        fromData,
                        {
                            headers: {
                                Authorization: `Bearer ${token}`,
                            },
                        }
                    );
                } else {
                    let res = await http.post("http://localhost:7070/user", fromData, {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    });
                }
                Swal.fire("Sukses", "Data berhasil disimpan", "success");
                setTimeout(() => {
                    window.location.href = "/user";
                }, 1000);
            } catch (e) {
                Swal.fire(
                    "Gagal",
                    `Data tidak berhasil disimpan dengan pesan error ${e.message}`,
                    "error"
                );
            }
        } else {
            this.setState({
                formIsError: true,
                errorMessage: errorMessage,
            });
        }
    }

    handleFileRead = async (event) => {
        const file = event.target.files[0];
        const base64 = await this.convertBase64(file);
        console.log(base64)
        this.setState({
            foto: base64,
        });
    };

    convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    <Sidebar />
                    <div className="content-body">
                        <div className="container-fluid">
                            <div className="card">
                                <div className="card-body">
                                    <div className="basic-form">
                                        <div className="mb3">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    {this.state.formIsError && (
                                                        <div className="alert alert-danger solid alert-dismissible fade show">
                                                            <svg
                                                                viewBox="0 0 24 24"
                                                                width="24 "
                                                                height="24"
                                                                stroke="currentColor"
                                                                strokeWidth="2"
                                                                fill="none"
                                                                strokeLinecap="round"
                                                                strokeLinejoin="round"
                                                                className="me-2"
                                                            >
                                                                <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                                                <line x1="15" y1="9" x2="9" y2="15"></line>
                                                                <line x1="9" y1="9" x2="15" y2="15"></line>
                                                            </svg>
                                                            {/* <strong>Error!</strong>  */}
                                                            {this.state.errorMessage}
                                                            <button
                                                                type="button"
                                                                className="btn-close"
                                                                data-bs-dismiss="alert"
                                                                aria-label="btn-close"
                                                            ></button>
                                                        </div>
                                                    )}
                                                </div>
                                                {this.state.id &&
                                                    <div className="mb-3 col-md-12">
                                                        <img src={this.state.foto} style={{ margin: '0 auto', display: 'block', height: '120px', width: 'auto' }} />
                                                    </div>
                                                }
                                                <div className="mb-3 col-md-12">
                                                    <div className="form-file">
                                                        <input
                                                            type="file"
                                                            accept="image/*"
                                                            className="form-file-input form-control"
                                                            onChange={(e) => {
                                                                this.handleFileRead(e);
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="mb-3 col-md-12">
                                                    <label className="form-label">Username</label>
                                                    <input
                                                        value={this.state.username}
                                                        onChange={(e) => {
                                                            this.setState({ username: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    />
                                                </div>
                                                {!this.state.id &&
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Password</label>
                                                        <input
                                                            value={this.state.password}
                                                            onChange={(e) => {
                                                                this.setState({ password: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="password"
                                                            placeholder=""
                                                        />
                                                    </div>
                                                }
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Nama Lengkap</label>
                                                    <input
                                                        value={this.state.nama}
                                                        onChange={(e) => {
                                                            this.setState({ nama: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">User Role</label>
                                                    <select
                                                        value={this.state.role_id}
                                                        onChange={(e) => {
                                                            this.setState({ role_id: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    >
                                                        {this.state.role && this.state.role.length > 0 && (
                                                            <>
                                                                {JSON.stringify(this.state.role)}
                                                                <option value={null}>-</option>
                                                                {this.state.role.map((d, i) => {
                                                                    return <option value={d.id}>{d.nama}</option>;
                                                                })}
                                                            </>
                                                        )}
                                                    </select>
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Jabatan</label>
                                                    <select
                                                        value={this.state.jabatan_id}
                                                        onChange={(e) => {
                                                            this.setState({ jabatan_id: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    >
                                                        {this.state.jabatan &&
                                                            this.state.jabatan.length > 0 && (
                                                                <>
                                                                    {JSON.stringify(this.state.jabatan)}
                                                                    <option value={null}>-</option>
                                                                    {this.state.jabatan.map((d, i) => {
                                                                        return (
                                                                            <option value={d.id}>{d.nama}</option>
                                                                        );
                                                                    })}
                                                                </>
                                                            )}
                                                    </select>
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Departemen</label>
                                                    <select
                                                        value={this.state.departemen_id}
                                                        onChange={(e) => {
                                                            this.setState({ departemen_id: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    >
                                                        {this.state.departemen &&
                                                            this.state.departemen.length > 0 && (
                                                                <>
                                                                    {JSON.stringify(this.state.departemen)}
                                                                    <option value={null}>-</option>
                                                                    {this.state.departemen.map((d, i) => {
                                                                        return (
                                                                            <option value={d.id}>{d.nama}</option>
                                                                        );
                                                                    })}
                                                                </>
                                                            )}
                                                    </select>
                                                </div>
                                                <div className="mb-3 col-md-12">
                                                    <label className="form-label">Jam Kerja</label>
                                                    <select
                                                        value={this.state.jadwal_id}
                                                        onChange={(e) => {
                                                            this.setState({ jadwal_id: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    >
                                                        {this.state.jadwal && this.state.jadwal.length > 0 && (
                                                            <>
                                                                {JSON.stringify(this.state.jadwal)}
                                                                <option value={null}>-</option>
                                                                {this.state.jadwal.map((d, i) => {
                                                                    return (
                                                                        <option value={d.id}>
                                                                            {d.nama} {d.jam_masuk} - {d.jam_keluar}
                                                                        </option>
                                                                    );
                                                                })}
                                                            </>
                                                        )}
                                                    </select>
                                                </div>
                                            </div>
                                            <a
                                                href="/user"
                                                style={{ marginRight: "10px" }}
                                                className="btn btn-xs btn-info"
                                            >
                                                Kembali
                                            </a>
                                            <button
                                                className="btn btn-xs btn-success"
                                                onClick={() => {
                                                    this.handleSave();
                                                }}
                                            >
                                                Simpan Data
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    );
}

export default Page;
