import constant from "../../components/constant";
import http from "../../components/http.service";
import React from "react";
import Head from "../../components/Head";
import Navbar from "../app/navbar";
import Sidebar from "../app/sidebar";
import Swal from "sweetalert2";
import Router from "next/router";
import { SketchPicker } from "react-color";

export async function getServerSideProps() {
  // Fetch data
  var data = {};
  return { props: { data } };
}

class PageComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageTitle: "Pengaturan Aplikasi",
      id: 1,
      nama: null,
      ikon: null,
      warnaSidebar: "#fff",
      warnaTopbar: "#fff",
      errorMessage: [],
      formIsError: false,
    };
  }

  async initData() {
    var query = Router.query;
    var token = localStorage.getItem("mts-token");
    try {
      let res = await http.get(`http://localhost:7070/setting/1`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (res.data) {
        this.setState({
          nama: res.data.nama,
          ikon: res.data.ikon,
          warnaSidebar: res.data.warnaSidebar ? res.data.warnaSidebar : '#fff',
          warnaTopbar: res.data.warnaTopbar ? res.data.warnaTopbar : '#fff',
          id: res.data.id,
        });
      }
    } catch (e) { }
  }

  async componentDidMount() {
    var token = localStorage.getItem("mts-token");
    try {
      let res = await http.get("http://localhost:7070/profile", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      // window.location.href="/app"
    } catch (e) {
      window.location.href = "/login";
    }
    this.initData();
  }

  async handleSave() {
    var errorMessage = "";
    if (!this.state.nama) {
      errorMessage += "nama diperlukan,";
    }
    if (errorMessage == "") {
      var fromData = {
        nama: this.state.nama,
        ikon: this.state.ikon,
        warnaTopbar: this.state.warnaTopbar,
        warnaSidebar: this.state.warnaSidebar,
        id: 1,
      };

      try {
        var token = localStorage.getItem("mts-token");
        let res = await http.post(`http://localhost:7070/setting`, fromData, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        Swal.fire("Sukses", "Data berhasil disimpan", "success");
        localStorage.setItem("mts-tema", JSON.stringify(fromData));

        window.location.reload();
      } catch (e) {
        Swal.fire(
          "Gagal",
          `Data tidak berhasil disimpan dengan pesan error ${e.message}`,
          "error"
        );
      }
    } else {
      this.setState({
        formIsError: true,
        errorMessage: errorMessage,
      });
    }
  }

  handleFileRead = async (event) => {
    const file = event.target.files[0];
    const base64 = await this.convertBase64(file);
    console.log(base64);
    this.setState({
      ikon: base64,
    });
  };

  convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  render() {
    return (
      <>
        <div id="main-wrapper">
          <Navbar pageTitle={this.state.pageTitle} />
          <Sidebar />
          <div className="content-body">
            <div className="container-fluid">
              <div className="card">
                <div className="card-body">
                  <div className="basic-form">
                    <div className="mb3">
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.formIsError && (
                            <div className="alert alert-danger solid alert-dismissible fade show">
                              <svg
                                viewBox="0 0 24 24"
                                width="24 "
                                height="24"
                                stroke="currentColor"
                                strokeWidth="2"
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                className="me-2"
                              >
                                <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                <line x1="15" y1="9" x2="9" y2="15"></line>
                                <line x1="9" y1="9" x2="15" y2="15"></line>
                              </svg>
                              {/* <strong>Error!</strong>  */}
                              {this.state.errorMessage}
                              <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="alert"
                                aria-label="btn-close"
                              ></button>
                            </div>
                          )}
                        </div>
                        <div className="mb-3 col-md-12">
                          <label className="form-label">Nama</label>
                          <input
                            value={this.state.nama}
                            onChange={(e) => {
                              this.setState({ nama: e.target.value });
                            }}
                            style={{ height: 36 }}
                            className="form-control input-default form-control-sm"
                            type="text"
                            placeholder=""
                          />
                        </div>
                        <div className="mb-3 col-md-12">
                          <img
                            src={this.state.ikon}
                            style={{
                              margin: "0 auto",
                              display: "block",
                              height: "100px",
                              width: "auto",
                            }}
                          />
                        </div>
                        <div className="mb-3 col-md-12">
                          <label className="form-label">Ikon</label>
                          <div className="form-file">
                            <input
                              type="file"
                              accept="image/*"
                              className="form-file-input form-control"
                              onChange={(e) => {
                                this.handleFileRead(e);
                              }}
                            />
                          </div>
                        </div>
                        <div className="mb-3 col-md-6">
                          <div className="example">
                            <p className="mb-1">Warna Sidebar</p>
                            <SketchPicker
                              color={this.state.warnaSidebar}
                              onChangeComplete={(c) => {
                                this.setState({ warnaSidebar: c.hex });
                              }}
                            />
                          </div>
                        </div>
                        <div className="mb-3 col-md-6">
                          <div className="example">
                            <p className="mb-1">Warna Topbar</p>
                            <SketchPicker
                              color={this.state.warnaTopbar}
                              onChangeComplete={(c) => {
                                this.setState({ warnaTopbar: c.hex });
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <button
                        className="btn btn-xs btn-success"
                        onClick={() => {
                          this.handleSave();
                        }}
                      >
                        Simpan Data
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

function Page({ data }) {
  return (
    <>
      <Head />
      <PageComponent data={data} />
    </>
  );
}

export default Page;
