import React from "react";

export default class Sidebar extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            menus: [
                    { name: 'Dashboard', url: '/dashboard-user', active: false, index: '', icon: '' },
                    { name: 'Proyek', url: '/proyek', active: false, index: '', icon: '' },
                    { name: 'Tugas / Issue', url: '/task', active: false, index: '', icon: '' },
                    { name: 'Kehadiran', url: '/absen', active: false, index: '', icon: '' },
            ],
            user: {},
            tema: {},
        }
    }

    componentDidMount() {
        this.setState({tema: localStorage.getItem('mts-tema')?JSON.parse(localStorage.getItem('mts-tema')):{},})
        this.setState({user: JSON.parse(localStorage.getItem('mts-user'))})
    }

    render() {
        return (
            <>
                <div className="deznav" style={{backgroundColor: this.state.tema.warnaTopbar?this.state.tema.warnaTopbar:'initial'}}>
                    <div className="deznav-scroll">
                        <div className="main-profile">
                            <div className="image-bx">
                                <img src={this.state.user.foto} alt="" />
                                <a><i className="fa fa-cog" aria-hidden="true"></i></a>
                            </div>
                            <h5 className="name"><span className="font-w400"></span> {this.state.user.nama}</h5>
                            {/* <p className="email">marquezzzz@mail.com</p> */}
                        </div>

                        <ul className="metismenu" id="menu">
                            <li className="nav-label first">Main Menu</li>
                            {/* <li><a className="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                                <i className="flaticon-144-layout"></i>
                                <span className="nav-text">Dashboard</span>
                            </a>
                                <ul aria-expanded="false">
                                    <li><a href="index.html">Dashboard Light</a></li>
                                    <li><a href="index-2.html">Dashboard Dark</a></li>
                                    <li><a href="my-wallets.html">Wallet</a></li>
                                    <li><a href="tranasactions.html">Transactions</a></li>
                                    <li><a href="coin-details.html">Coin Details</a></li>
                                    <li><a href="portofolio.html">Portofolio</a></li>
                                    <li><a href="market-capital.html">Market Capital</a></li>
                                </ul>
                            </li> */}
                            {this.state.menus.map((d, i)=> {
                                return (
                                    <li key={i}>
                                        <a href={d.url} className="ai-icon" aria-expanded="false">
                                            <i className="flaticon-049-copy"></i>
                                            <span className="nav-text">{d.name}</span>
                                        </a>
                                    </li>
                                )
                            })}
                            {/* <li>
                                <a href="widget-basic.html" className="ai-icon" aria-expanded="false">
                                    <i className="flaticon-381-settings-2"></i>
                                    <span className="nav-text">Widget</span>
                                </a>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </>
        )
    }
}