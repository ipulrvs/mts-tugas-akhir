import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from './navbar'
import Sidebar from './sidebar'

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class AppComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "Dashboard"
        }
    }

    async componentDidMount() {
        try {
            var token = localStorage.getItem('mts-token')
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        try {
            let res1 = await http.get(`http://localhost:7070/setting`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            localStorage.setItem("mts-tema", JSON.stringify(res1.data.data[0]));
        } catch(e){}
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    <Sidebar />
                    <div className="content-body">

                    </div>
                </div>
            </>
        )
    }
}

function App({ data }) {
    return (
        <>
            <Head />
            <AppComponent data={data} />
        </>
    )
}

export default App
