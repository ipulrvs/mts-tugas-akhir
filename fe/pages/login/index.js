import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import { withRouter, useRouter } from 'next/router'


export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}


class LoginComponent extends React.Component {

    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            username: null,
            password: null,
            formIsError: false,
            errorMessage: ""
        }
    }

    async componentDidMount(){
        try {
          var token = localStorage.getItem('mts-token')
          let res = await http.get('http://localhost:7070/profile', {
            headers: {
              'Authorization': `Bearer ${token}`
            }
          })
          window.location.href="/app"
        } catch(e){
            console.log(e)
          // window.location.href="/login"
        }
      }

    async login() {
        if(this.state.username && this.state.password){
            try {
                let res = await http.post('http://localhost:7070/auth/login', {
                    username: this.state.username,
                    password: this.state.password,
                })
                let profile = await http.get('http://localhost:7070/profile', {
                    headers: {
                      'Authorization': `Bearer ${res.data.access_token}`
                    }
                })
                localStorage.setItem('mts-user', JSON.stringify(res.data.user)) 
                localStorage.setItem('mts-token', res.data.access_token)
                setTimeout(()=> {
                   if(res.data.user.role.nama == "Super Admin"){
                    window.location.href = "/dashboard"
                   } else {
                    window.location.href = "/app-user"
                   }
                }, 200)
                // this.props.router.push("/app")
            } catch(e){
                this.setState({
                    formIsError: true,
                    errorMessage: "Data user tidak ditemukan!"
                })
            }
        } else {
            this.setState({
                formIsError: true,
                errorMessage: "Username dan Password diperlukan!"
            })
        }
    }

    render() {
        return (
            <div className="vh-100">
                <div className="authincation h-100">
                    <div className="container h-100">
                        <div className="row justify-content-center h-100 align-items-center">
                            <div className="col-md-6">
                                <div className="authincation-content">
                                    <div className="row no-gutters">
                                        <div className="col-xl-12">
                                            <div className="auth-form">
                                                {/* <div className="text-center mb-4">
                                                    <img src="/zenix/images/logo-full-black.png" alt="" />
                                                </div> */}
                                                <h4 className="text-center mb-4">Masukan Akun Anda</h4>
                                                {this.state.formIsError &&
                                                    <div className="alert alert-danger solid alert-dismissible fade show">
                                                        <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" className="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                        {/* <strong>Error!</strong>  */}
                                                        {this.state.errorMessage}
                                                        <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                                        </button>
                                                    </div>
                                                }
                                                <div>
                                                    <div className="form-group">
                                                        <label className="mb-1"><strong>Username</strong></label>
                                                        <input type="text" className="form-control" required value={this.state.username} onChange={(e)=> this.setState({ username: e.target.value })} />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="mb-1"><strong>Password</strong></label>
                                                        <input type="password" className="form-control" required value={this.state.password} onChange={(e)=> this.setState({ password: e.target.value })} />
                                                    </div>
                                                    <div className="text-center">
                                                        <button onClick={() => this.login()} className="btn btn-primary btn-block">Sign In</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function Login({ data }) {
    const router = useRouter();
    return (
        <>
            <Head />
            <LoginComponent data={data} router={router} />
        </>
    )
}

export default Login
