import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from '../app/navbar'
import Sidebar from '../app/sidebar'
import Swal from 'sweetalert2'

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class PageComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "",
            data: [],
            total: [],
            limit: 20,
            totalPage: 0,
            pageNo: 0,
            pageRangeNo: 0,
            pageRange: [],
            showSearch: false,
            nama: null,
            keterangan: null,
        }
    }

    async initData(params) {
        if (!params) params = {}
        var token = localStorage.getItem('mts-token')
        if (params && params.page) {
            params.page = this.state.pageNo
        } else {
            params.page = this.state.pageNo
        }
        try {
            var getData = await http.get('http://localhost:7070/role', {
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                params: params
            })


            // Pagination
            var totalData = getData.data.total
            var limitData = this.state.limit
            var totalPage = Math.ceil(totalData / limitData)
            var pageRange = []
            var pageRangeNo = this.state.pageRangeNo
            var pageNo = this.state.pageNo
            var initPage = 0
            while (initPage < totalPage) {
                pageRange.push(initPage)
                initPage++;
            }
            var pageRangeLimit = 5
            var newPageRange = []
            //newPageRange = pageRange.slice((pageRangeNo*pageRangeLimit)*pageNo, (pageRangeNo + 1) * pageRangeLimit)
            console.log(newPageRange, pageRangeNo, "TELL ME")
            // Pagination
            this.setState({
                data: getData.data.data,
                total: getData.data.total,
                showSearch: false,
                pageRangeNo: 0,
                pageRange: newPageRange,
                totalPage: totalPage
            })
        } catch (e) {
            console.log(e)
        }
    }

    async componentDidMount() {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        this.initData()
    }

    async removeItem(d) {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.delete(`http://localhost:7070/role/${d.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) { }
    }

    async edit(d) {
        window.location.href = `/role/${d.id}`
    }

    async hapus(data) {
        Swal.fire({
            title: 'Hapus Data',
            text: "Apa anda yakin akan melakukan proses ini ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(async (result) => {
            if (result.isConfirmed) {
                await this.removeItem(data)
                this.initData()
                Swal.fire(
                    'Berhasil',
                    'Data berhasil dihapus.',
                    'success'
                )
            }
        })
    }

    async handlePage(d) {
        this.setState({
            pageNo: d
        }, () => {
            this.initData()
        })
    }

    async handleCari() {
        var nama = this.state.nama
        var keterangan = this.state.keterangan
        var where = {}
        if (nama) where.nama = nama
        if (keterangan) where.keterangan = keterangan
        this.initData({
            where: where
        })
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    <Sidebar />
                    <div className="content-body">
                        <div className='container-fluid'>
                            {this.state.showSearch &&
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title">Pencarian</h4>
                                    </div>
                                    <div className="card-body">
                                        <div className="basic-form">
                                            <div className="row">
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Nama Role</label>
                                                    <input value={this.state.nama} onChange={(e) => { this.setState({ nama: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Keterangan</label>
                                                    <input value={this.state.keterangan} onChange={(e) => { this.setState({ keterangan: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                </div>
                                            </div>
                                            <button className="btn btn-xs btn-primary" onClick={(e) => this.handleCari()}>Cari Data</button>
                                        </div>
                                    </div>
                                </div>
                            }
                            <div className='card'>
                                <div className="card-header d-sm-flex d-block">
                                    <div className="me-auto mb-sm-0 mb-3">
                                        <h4 className="card-title mb-2">Master Role</h4>
                                        <span>Menampilkan role yang tersedia pada aplikasi</span>
                                    </div>
                                    {/* <a onClick={() => {
                                        window.location.href = window.location.href + '/new'
                                    }} className="btn btn-xs btn-success">+ Tambah Data</a> */}
                                    <a style={{ marginLeft: 10 }} onClick={() => {
                                        window.location.href = window.location.href
                                    }} className="btn btn-xs btn-info">Reload Data</a>
                                    <a href="javascript:void(0);" style={{ marginLeft: 10 }}
                                        onClick={() => { this.setState({ showSearch: !this.state.showSearch }) }}
                                        className="btn btn-xs btn-info">Pencarian Data</a>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table style-1" id="ListDatatableView">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>NAMA ROLE</th>
                                                    <th>KETERANGAN</th>
                                                    {/* <th>ACTION</th> */}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.data && this.state.data.length > 0 &&
                                                    <>
                                                        {this.state.data.map((d, index) => {
                                                            return (
                                                                <tr key={index}>
                                                                    <td>{(index + 1) + this.state.pageNo}</td>
                                                                    <td>{d.nama}</td>
                                                                    <td>{d.keterangan}</td>
                                                                    <td style={{ width: "150px" }}>
                                                                        <div className="d-flex action-button">
                                                                            {/* <a href="javascript:void(0);" onClick={() => { this.edit(d) }} className="btn btn-info btn-xs light px-2">
                                                                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#fff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                                                                                </svg>
                                                                            </a> */}
                                                                            {/* <a href="javascript:void(0);" onClick={() => { this.hapus(d) }} className="ms-2 btn btn-xs px-2 light btn-danger">
                                                                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path d="M3 6H5H21" stroke="#fff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                                                                                    <path d="M8 6V4C8 3.46957 8.21071 2.96086 8.58579 2.58579C8.96086 2.21071 9.46957 2 10 2H14C14.5304 2 15.0391 2.21071 15.4142 2.58579C15.7893 2.96086 16 3.46957 16 4V6M19 6V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V6H19Z" stroke="#fff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                                                                                </svg>
                                                                            </a> */}
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })}
                                                    </>
                                                }
                                                {/* <tr>
                                                    
                                                    <td><h6>1.</h6></td>
                                                    <td></td> */}
                                                {/* <td>
                                                        <div className="media style-1">
                                                            <img src="/zenix/images/users/pic1.jpg" className="img-fluid me-3" alt="" />
                                                            <div className="media-body">
                                                                <h6>John Doe</h6>
                                                                <span>johndoe@gmail.com</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <h6>England</h6>
                                                            <span>COde:En</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <h6 className="text-primary">10/21/2016</h6>
                                                            <span>Paid</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Abbott-Jacobs
                                                    </td>
                                                    <td><span className="badge badge-warning light">Pending</span></td>
                                                    <td>
                                                        <div className="d-flex action-button">
                                                            <a href="javascript:void(0);" className="btn btn-info btn-xs light px-2">
                                                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#fff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                                                                </svg>
                                                            </a>
                                                            <a href="javascript:void(0);" className="ms-2 btn btn-xs px-2 light btn-danger">
                                                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M3 6H5H21" stroke="#fff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                                                                    <path d="M8 6V4C8 3.46957 8.21071 2.96086 8.58579 2.58579C8.96086 2.21071 9.46957 2 10 2H14C14.5304 2 15.0391 2.21071 15.4142 2.58579C15.7893 2.96086 16 3.46957 16 4V6M19 6V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V6H19Z" stroke="#fff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                                                                </svg>

                                                            </a>
                                                        </div>
                                                    </td> */}
                                                {/* </tr> */}
                                            </tbody>
                                        </table>
                                        <div className="dataTables_info" id="ListDatatableView_info" role="status" aria-live="polite">Menampilkan {this.state.limit * (this.state.pageNo) + 1} dari {this.state.total} data</div>
                                        <nav style={{ float: 'right' }}>
                                            <ul className="pagination pagination-sm pagination-gutter pagination-primary no-bg">
                                                {this.state.pageNo != 0 &&
                                                    <li className="page-item page-indicator">
                                                        <a className="page-link" style={{ cursor: 'pointer' }} onClick={() => {
                                                            this.setState({
                                                                pageNo: this.state.pageNo - 1
                                                            }, () => {
                                                                this.initData()
                                                            })
                                                        }}>
                                                            <i className="la la-angle-left"></i>
                                                        </a>
                                                    </li>
                                                }
                                                {this.state.pageRange && this.state.pageRange.length > 0 &&
                                                    <>
                                                        {this.state.pageRange.map((d, key) => {
                                                            var className = "page-item"
                                                            if (d == this.state.pageNo) className = "page-item active"
                                                            return <li style={{ cursor: "pointer" }} className={className} key={key} onClick={() => this.handlePage(d)}><a className="page-link">{d + 1}</a></li>
                                                        })}
                                                    </>
                                                }
                                                {this.state.pageNo != (this.state.totalPage -1) &&
                                                    <li className="page-item page-indicator">
                                                        <a className="page-link" style={{ cursor: 'pointer' }} onClick={() => {
                                                            this.setState({
                                                                pageNo: this.state.pageNo + 1,
                                                                pageRangeNo : this.state.pageRangeNo + 1
                                                            }, () => {
                                                                this.initData()
                                                            })
                                                        }}>
                                                            <i className="la la-angle-right"></i>
                                                        </a>
                                                    </li>
                                                }
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    )
}

export default Page
