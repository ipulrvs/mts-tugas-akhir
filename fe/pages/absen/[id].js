import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from '../app/navbar'
import Sidebar from '../app/sidebar'
import Swal from 'sweetalert2'
import Router from 'next/router'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class PageComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "Formulir Kehadiran",
            id: null,
            lembur: 0,
            terlambat: 0,
            checkin: new Date(),
            checkout: new Date(),
            tipe_kehadiran: 'HADIR',
            pengguna_id: null,
            pengguna: [],
            errorMessage: [],
            formIsError: false
        }
    }

    async initData() {
        var query = Router.query
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get(`http://localhost:7070/absen/${query.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            var data = {
                nama: res.data.nama,
                alasan: res.data.alasan,
                tipe_kehadiran: res.data.tipe_kehadiran,
                lembur: res.data.lembur,
                terlambat: res.data.terlambat,
                checkin: new Date(res.data.checkin),
                checkout: new Date(res.data.checkout),
                id: res.data.id
            }
            try { data.pengguna_id = res.data.user.id } catch (e) { }
            this.setState(data)
        } catch (e) { }
    }

    async componentDidMount() {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/user`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                pengguna: res.data.data,
            });
        } catch (e) { }
        this.initData()
    }

    async handleSave() {
        var errorMessage = ""
        if (!this.state.pengguna_id) {
            errorMessage += "pengguna diperlukan,"
        }
        if (!this.state.alasan) {
            errorMessage += " keterangan diperlukan,"
        }
        if (!this.state.checkin) {
            errorMessage += " checkin diperlukan,"
        }
        if (!this.state.checkout) {
            errorMessage += " checkout diperlukan,"
        }
        if (!this.state.tipe_kehadiran) {
            errorMessage += " tipe kehadiran diperlukan,"
        }
        if (errorMessage == "") {
            var fromData = {
                nama: this.state.nama,
                alasan: this.state.alasan,
                checkin: this.state.checkin,
                checkout: this.state.checkout,
                terlambat: this.state.terlambat,
                lembur: this.state.lembur,
                tipe_kehadiran: this.state.tipe_kehadiran,
                user: {
                    id: this.state.pengguna_id
                },
                id: this.state.id
            }
            try {
                var token = localStorage.getItem('mts-token')
                if (this.state.id) {
                    let res = await http.put(`http://localhost:7070/absen/${this.state.id}`, fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                } else {
                    let res = await http.post('http://localhost:7070/absen', fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                }
                Swal.fire('Sukses', 'Data berhasil disimpan', 'success')
                setTimeout(() => {
                    window.location.href = "/absen"
                }, 1000)
            } catch (e) {
                Swal.fire('Gagal', `Data tidak berhasil disimpan dengan pesan error ${e.message}`, 'error')
            }
        } else {
            this.setState({
                formIsError: true,
                errorMessage: errorMessage
            })
        }
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    <Sidebar />
                    <div className="content-body">
                        <div className='container-fluid'>
                            <div className="card">
                                <div className="card-body">
                                    <div className="basic-form">
                                        <div className='mb3'>
                                            <div className="row">
                                                <div className='col-md-12'>
                                                    {this.state.formIsError &&
                                                        <div className="alert alert-danger solid alert-dismissible fade show">
                                                            <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" className="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                            {/* <strong>Error!</strong>  */}
                                                            {this.state.errorMessage}
                                                            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                                            </button>
                                                        </div>
                                                    }
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Checkin</label>
                                                    <DatePicker
                                                        className="form-control input-default form-control-sm"
                                                        selected={this.state.checkin}
                                                        onChange={(e) => {
                                                            this.setState({ checkin: e })
                                                        }}
                                                        showTimeSelect
                                                        dateFormat="Pp"
                                                    />
                                                    {/* <input value={this.state.checkin} onChange={(e) => { this.setState({ checkin: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="datetime-local" placeholder="" /> */}
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Checkout</label>
                                                    <DatePicker
                                                        className="form-control input-default form-control-sm"
                                                        selected={this.state.checkout}
                                                        onChange={(e) => {
                                                            this.setState({ checkout: e })
                                                        }}
                                                        showTimeSelect
                                                        dateFormat="Pp"
                                                    />
                                                    {/* <input value={this.state.checkout} onChange={(e) => { this.setState({ checkout: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="datetime-local" placeholder="" /> */}
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Terlambat (Minute)</label>
                                                    <input value={this.state.terlambat} onChange={(e) => { this.setState({ terlambat: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" placeholder="" />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Lembur (Minute)</label>
                                                    <input value={this.state.lembur} onChange={(e) => { this.setState({ lembur: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" placeholder="" />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Pengguna</label>
                                                    <select
                                                        value={this.state.pengguna_id}
                                                        onChange={(e) => {
                                                            this.setState({ pengguna_id: e.target.value });
                                                        }}
                                                        style={{ height: 36 }}
                                                        className="form-control input-default form-control-sm"
                                                        type="text"
                                                        placeholder=""
                                                    >
                                                        {this.state.pengguna &&
                                                            this.state.pengguna.length > 0 && (
                                                                <>
                                                                    {JSON.stringify(this.state.pengguna)}
                                                                    <option value={null}>-</option>
                                                                    {this.state.pengguna.map((d, i) => {
                                                                        return (
                                                                            <option value={d.id}>{d.nama}</option>
                                                                        );
                                                                    })}
                                                                </>
                                                            )}
                                                    </select>
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Jenis Kehadiran</label>
                                                    <select value={this.state.tipe_kehadiran} onChange={(e) => { this.setState({ tipe_kehadiran: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" placeholder="">
                                                        <option value={null}>-</option>
                                                        <option value="HADIR">HADIR</option>
                                                        <option value="HADIR DAN TERLAMBAT">HADIR DAN TERLAMBAT</option>
                                                        <option value="HADIR DAN LEMBUR">HADIR DAN LEMBUR</option>
                                                        <option value="CUTI">CUTI</option>
                                                        <option value="CUTI SETENGAH HARI">CUTI SETENGAH HARI</option>
                                                        <option value="IJIN">IJIN</option>
                                                        <option value="IJIN SETENGAH HARI">IJIN SETENGAH HARI</option>
                                                    </select>
                                                </div>
                                                <div className="mb-3 col-md-12">
                                                    <label className="form-label">Keterangan</label>
                                                    <textarea value={this.state.alasan} onChange={(e) => { this.setState({ alasan: e.target.value }) }} style={{ height: 200 }} className="form-control input-default form-control-sm" type="text" placeholder="">
                                                    </textarea>
                                                </div>
                                            </div>
                                            <a href="/absen" style={{ marginRight: '10px' }} className="btn btn-xs btn-info">Kembali</a>
                                            <button className="btn btn-xs btn-success" onClick={() => { this.handleSave() }}>Simpan Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    )
}

export default Page
