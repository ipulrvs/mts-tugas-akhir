import Board from "react-trello";
import constant from "../../../components/constant";
import http from "../../../components/http.service";
import React from "react";
import Head from "../../../components/Head";
import Navbar from "../../app/navbar";
import Sidebar from "../../app/sidebar-proyek";
import SidebarUserProyek from '../../app/sidebar-proyek-user'
import Swal from "sweetalert2";
import Router from "next/router";

export async function getServerSideProps() {
  // Fetch data
  var data = {};
  return { props: { data } };
}

class PageComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isUser: false,
      pageTitle: "Formulir Sprint",
      id: null,
      nama: null,
      keterangan: null,
      status: "DRAFT",
      sprint_dimulai: null,
      sprint_berakhir: null,
      proyek_id: null,
      errorMessage: [],
      formIsError: false,
      taskMap: {},
      kolomMap: {},
      data: {
        lanes: [
          {
            id: "lane1",
            title: "Planned Tasks",
            label: "2/2",
            cards: [
              {
                id: "Card1",
                title: "Write Blog",
                description: "Can AI make memes",
                label: "30 mins",
                draggable: false,
              },
              {
                id: "Card2",
                title: "Pay Rent",
                description: "Transfer via NEFT",
                label: "5 mins",
                metadata: { sha: "be312a1" },
              },
            ],
          },
          {
            id: "lane2",
            title: "Completed",
            label: "0/0",
            cards: [],
          },
        ],
      },
    };
  }

  async initData() {
    try {
      var user = JSON.parse(localStorage.getItem('mts-user'))
      if (user.role.nama == "User") {
          this.setState({ isUser: true, tugas_dilaporkan_oleh_id: user.id })
      }
  } catch (e) { }
    var query = Router.query;
    var token = localStorage.getItem("mts-token");
    try {
      let res = await http.get(`http://localhost:7070/sprint/${query.id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      let kolom = await http.get(`http://localhost:7070/kolom`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          proyek_id: res.data.proyek_id,
          limit: 9999999999,
          sort: "ASC",
        },
      });

      let task = await http.get(`http://localhost:7070/task`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          sprint_id: query.id,
          limit: 9999999999,
          sort: "ASC",
        },
      });

      let lanes = kolom.data.data.map((d) => {
        return {
          id: d.id,
          title: d.nama,
          draggable: false,
          label: "2/2",
          cards: task.data.data
            .filter((e) => e.status.id == d.id)
            .map((d) => {
              return {
                id: d.id,
                title: d.nama,
                description: (
                  <div
                    className="Container"
                    dangerouslySetInnerHTML={{ __html: d.keterangan + d.tugas_dikerjakan_oleh.nama }}
                  ></div>
                ),
                label: d.nama,
                metadata: { data: d },
              };
            }),
        };
      });

      this.setState({
        pageTitle: "SPRINT | " + res.data.nama,
        nama: res.data.nama,
        keterangan: res.data.keterangan,
        status: res.data.status,
        sprint_dimulai: res.data.sprint_dimulai,
        sprint_berakhir: res.data.sprint_berakhir,
        proyek_id: res.data.proyek_id,
        id: res.data.id,
        data: {
          lanes: lanes,
        },
        taskMap: task.data.data.reduce((pv, cv) => {
          pv[cv.id] = cv;
          return pv;
        }, {}),
        kolomMap: kolom.data.data.reduce((pv, cv) => {
          pv[cv.id] = cv;
          return pv;
        }, {}),
      });
    } catch (e) {
      console.log(e.message);
    }
  }

  async componentDidMount() {
    var token = localStorage.getItem("mts-token");
    try {
      let res = await http.get("http://localhost:7070/profile", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      // window.location.href="/app"
    } catch (e) {
      window.location.href = "/login";
    }
    this.initData();
  }

  async handleSave() {
    try {
      var token = localStorage.getItem("mts-token");
      let res = await http.get(`http://localhost:7070/sprint/finish/${this.state.id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      Swal.fire("Info", `Sprint telah di update`, "info");
      window.location.href = `/sprint`;
    } catch (e) {
      Swal.fire("Warning", e.response.data.error, "warning");
    }
  }

  async handleMoveLane(toLaneId, cardId) {
    var token = localStorage.getItem("mts-token");
    let res = await http.put(
      `http://localhost:7070/task/${cardId}`,
      {
        ...this.state.taskMap[cardId],
        status: this.state.kolomMap[toLaneId],
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    // Swal.fire(
    //   "Info",
    //   `Task telah berhasil di update`,
    //   "info"
    // );

    this.initData();
  }

  render() {
    return (
      <>
        <div id="main-wrapper">
          <Navbar pageTitle={this.state.pageTitle} />
          {!this.state.isUser &&
                        <Sidebar />
                    }
                    {this.state.isUser &&
                        <SidebarUserProyek />
                    }
          <div className="content-body">
            <div className="container-fluid">
              <div className="card">
                <div className="card-body">
                  <div className="basic-form">
                    <div className="mb3">
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.formIsError && (
                            <div className="alert alert-danger solid alert-dismissible fade show">
                              <svg
                                viewBox="0 0 24 24"
                                width="24 "
                                height="24"
                                stroke="currentColor"
                                strokeWidth="2"
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                className="me-2"
                              >
                                <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                <line x1="15" y1="9" x2="9" y2="15"></line>
                                <line x1="9" y1="9" x2="15" y2="15"></line>
                              </svg>
                              {/* <strong>Error!</strong>  */}
                              {this.state.errorMessage}
                              <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="alert"
                                aria-label="btn-close"
                              ></button>
                            </div>
                          )}
                        </div>
                        <Board
                          style={{backgroundColor: 'white'}} 
                          data={this.state.data}
                          onCardMoveAcrossLanes={(
                            fromLaneId,
                            toLaneId,
                            cardId,
                            index
                          ) => {
                            this.handleMoveLane(fromLaneId, cardId);
                          }}
                        />
                      </div>
                      <br />
                      <a
                        href={"/sprint"}
                        style={{ marginRight: "10px" }}
                        className="btn btn-xs btn-info"
                      >
                        Kembali
                      </a>
                      {/* <button
                        className="btn btn-xs btn-success"
                        onClick={() => {
                          this.handleSave();
                        }}
                      >
                        Finish Sprint
                      </button> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

function Page({ data }) {
  return (
    <>
      <Head />
      <PageComponent data={data} />
    </>
  );
}

export default Page;
