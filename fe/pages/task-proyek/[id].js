import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from '../app/navbar'
import Sidebar from '../app/sidebar-proyek'
import SidebarUserProyek from '../app/sidebar-proyek-user'
import Swal from 'sweetalert2'
import Router from 'next/router'
import moment from 'moment'

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class PageComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "Master Tugas | Formulir Tugas",
            id: null,
            nama: null,
            keterangan: null,
            status_id: null,
            timeline_id: null,
            sprint_id: null,
            tipe_tugas_id: null,
            tugas_dilaporkan_oleh_id: null,
            tugas_dikerjakan_oleh_id: null,
            prioritas: null,
            proyek_id: null,
            status: [],
            tipe_tugas: [],
            timeline: [],
            sprint: [],
            tugas_dilaporkan_oleh: [],
            tugas_dikerjakan_oleh: [],
            estimasi_awal: 0,
            estimasi_dikerjakan: 0,
            point: 0,
            errorMessage: [],
            formIsError: false,
            isUser: false,
            history: [],
        }
    }

    async initData() {
        try {
            var user = JSON.parse(localStorage.getItem('mts-user'))
            if (user.role.nama == "User") {
                this.setState({ isUser: true, tugas_dilaporkan_oleh_id: user.id })
            }
        } catch (e) { }
        var query = Router.query
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get(`http://localhost:7070/task/${query.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            var data = {
                prioritas: res.data.prioritas,
                nama: res.data.nama,
                keterangan: res.data.keterangan,
                proyek_id: res.data.proyek_id,
                estimasi_awal: res.data.estimasi_awal,
                estimasi_dikerjakan: res.data.estimasi_dikerjakan,
                id: res.data.id
            }
            try {
                data.status_id = res.data.status.id
            } catch (e) { }
            try {
                data.tipe_tugas_id = res.data.tipe_tugas.id
            } catch (e) { }
            try {
                data.timeline_id = res.data.timeline.id
            } catch (e) { }
            try {
                data.sprint_id = res.data.sprint.id
            } catch (e) { }
            try {
                data.tugas_dilaporkan_oleh_id = res.data.tugas_dilaporkan_oleh.id
            } catch (e) { }
            try {
                data.tugas_dikerjakan_oleh_id = res.data.tugas_dikerjakan_oleh.id
            } catch (e) { }
            this.setState(data)
            var keterangan = $('#summernote').summernote('code', res.data.keterangan);
            var keterangan = $('#summernote-comment').summernote('code', '');

            this.setState({
                tugas_dilaporkan_oleh: res.data.data,
                tugas_dikerjakan_oleh: res.data.data
            });

            let resComment = await http.get(`http://localhost:7070/task_history`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: {
                    task_id: query.id,
                    limit: 90000000,
                }
            });

            this.setState({history: resComment.data.data})
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/kolom`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                status: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/tipe_tugas`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                tipe_tugas: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/timeline`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                timeline: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/sprint`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                sprint: res.data.data,
            });
        } catch (e) { }
        try {
            var params = {}
            params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
            let res = await http.get(`http://localhost:7070/user`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: params
            });
            this.setState({
                tugas_dilaporkan_oleh: res.data.data,
                tugas_dikerjakan_oleh: res.data.data
            });
        } catch (e) { }
    }

    async componentDidMount() {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        this.initData()
        $(document).ready(function () {
            $('#summernote').summernote({
                minHeight: 350
            });
        });
    }

    async handleSave() {
        var keterangan = $('#summernote').summernote('code');
        console.log(keterangan)
        var errorMessage = ""
        if (!this.state.nama) {
            errorMessage += "nama diperlukan,"
        }
        if (!this.state.status_id) {
            errorMessage += " status diperlukan,"
        }
        if (!keterangan || keterangan.length <= 11) {
            errorMessage += " keterangan diperlukan,"
        }
        if (!this.state.tipe_tugas_id) {
            errorMessage += " tipe tugas diperlukan,"
        }
        if (errorMessage == "") {
            var fromData = {
                prioritas: this.state.prioritas,
                nama: this.state.nama,
                keterangan: keterangan,
                id: this.state.id,
                estimasi_awal: this.state.estimasi_awal,
                estimasi_dikerjakan: this.state.estimasi_awal,
                status: {
                    id: this.state.status_id
                },
                tipe_tugas: {
                    id: this.state.tipe_tugas_id
                },
                timeline: {
                    id: this.state.timeline_id
                },
                sprint: {
                    id: this.state.sprint_id
                },
                tugas_dilaporkan_oleh: {
                    id: this.state.tugas_dilaporkan_oleh_id
                },
                tugas_dikerjakan_oleh: {
                    id: this.state.tugas_dikerjakan_oleh_id
                },
                proyek_id: this.state.proyek_id ? this.state.proyek_id : JSON.parse(localStorage.getItem("mts-proyek")).id
            }
            try {
                var token = localStorage.getItem('mts-token')
                if (this.state.id) {
                    let res = await http.put(`http://localhost:7070/task/${this.state.id}`, fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                } else {
                    let res = await http.post('http://localhost:7070/task', fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                }
                Swal.fire('Sukses', 'Data berhasil disimpan', 'success')
                setTimeout(() => {
                    window.location.href = "/task-proyek"
                }, 1000)
            } catch (e) {
                Swal.fire('Gagal', `Data tidak berhasil disimpan dengan pesan error ${e.message}`, 'error')
            }
        } else {
            this.setState({
                formIsError: true,
                errorMessage: errorMessage
            })
        }
    }

     handlePostComment = async (e)=> {
        e.preventDefault()
        var token = localStorage.getItem('mts-token')
        var keterangan = $('#summernote-comment').summernote('code');
        var user = JSON.parse(localStorage.getItem('mts-user'));
        let res = await http.post('http://localhost:7070/task_history', {
            keterangan: keterangan,
            task: this.state,
            dibuat_oleh: user.username,
            dibuat_tgl: new Date(),
            diupdate_oleh: user.username,
            diupdate_tgl: new Date()
        }, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        this.initData()
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    {!this.state.isUser &&
                        <Sidebar />
                    }
                    {this.state.isUser &&
                        <SidebarUserProyek />
                    }
                    <div className="content-body">
                        <div className='container-fluid'>
                            <div className="card">
                                <div className="card-body">
                                    <div className="basic-form">
                                        <div className='mb3'>
                                            <div className="row">
                                                <div className='col-md-12'>
                                                    {this.state.formIsError &&
                                                        <div className="alert alert-danger solid alert-dismissible fade show">
                                                            <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" className="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                            {/* <strong>Error!</strong>  */}
                                                            {this.state.errorMessage}
                                                            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                                            </button>
                                                        </div>
                                                    }
                                                </div>
                                                <div className="mb-3 col-md-8">
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Judul Task dan Issue</label>
                                                        <input value={this.state.nama} onChange={(e) => { this.setState({ nama: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Keterangan</label>
                                                        <div id="summernote"></div>
                                                    </div>
                                                </div>
                                                <div className="mb-3 col-md-4 md-grid row">
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Status</label>
                                                        <select
                                                            value={this.state.status_id}
                                                            onChange={(e) => {
                                                                this.setState({ status_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.status &&
                                                                this.state.status.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.status)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.status.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Tipe Tugas</label>
                                                        <select
                                                            value={this.state.tipe_tugas_id}
                                                            onChange={(e) => {
                                                                this.setState({ tipe_tugas_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.tipe_tugas &&
                                                                this.state.tipe_tugas.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.tipe_tugas)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.tipe_tugas.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Timeline</label>
                                                        <select
                                                            value={this.state.timeline_id}
                                                            onChange={(e) => {
                                                                this.setState({ timeline_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.timeline &&
                                                                this.state.timeline.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.timeline)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.timeline.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Sprint</label>
                                                        <select
                                                            value={this.state.sprint_id}
                                                            onChange={(e) => {
                                                                this.setState({ sprint_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.sprint &&
                                                                this.state.sprint.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.sprint)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.sprint.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-12">
                                                        <label className="form-label">Dikerjakan Oleh</label>
                                                        <select
                                                            value={this.state.tugas_dikerjakan_oleh_id}
                                                            onChange={(e) => {
                                                                this.setState({ tugas_dikerjakan_oleh_id: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            {this.state.tugas_dikerjakan_oleh &&
                                                                this.state.tugas_dikerjakan_oleh.length > 0 && (
                                                                    <>
                                                                        {JSON.stringify(this.state.tugas_dikerjakan_oleh)}
                                                                        <option value={null}>-</option>
                                                                        {this.state.tugas_dikerjakan_oleh.map((d, i) => {
                                                                            return (
                                                                                <option value={d.id}>{d.nama}</option>
                                                                            );
                                                                        })}
                                                                    </>
                                                                )}
                                                        </select>
                                                    </div>
                                                    {!this.state.isUser &&
                                                        <div className="mb-3 col-md-12">
                                                            <label className="form-label">Dilaporkan Oleh</label>
                                                            <select
                                                                value={this.state.tugas_dilaporkan_oleh_id}
                                                                onChange={(e) => {
                                                                    this.setState({ tugas_dilaporkan_oleh_id: e.target.value });
                                                                }}
                                                                style={{ height: 36 }}
                                                                className="form-control input-default form-control-sm"
                                                                type="text"
                                                                placeholder=""
                                                            >
                                                                {this.state.tugas_dilaporkan_oleh &&
                                                                    this.state.tugas_dilaporkan_oleh.length > 0 && (
                                                                        <>
                                                                            {JSON.stringify(this.state.tugas_dilaporkan_oleh)}
                                                                            <option value={null}>-</option>
                                                                            {this.state.tugas_dilaporkan_oleh.map((d, i) => {
                                                                                return (
                                                                                    <option value={d.id}>{d.nama}</option>
                                                                                );
                                                                            })}
                                                                        </>
                                                                    )}
                                                            </select>
                                                        </div>
                                                     }
                                                     {this.state.isUser &&
                                                        <div className="mb-3 col-md-12">
                                                            <label className="form-label">Dilaporkan Oleh</label>
                                                            <select
                                                                value={this.state.tugas_dilaporkan_oleh_id}
                                                                onChange={(e) => {
                                                                    this.setState({ tugas_dilaporkan_oleh_id: e.target.value });
                                                                }}
                                                                style={{ height: 36 }}
                                                                className="form-control input-default form-control-sm"
                                                                type="text"
                                                                placeholder=""
                                                                disabled
                                                            >
                                                                {this.state.tugas_dilaporkan_oleh &&
                                                                    this.state.tugas_dilaporkan_oleh.length > 0 && (
                                                                        <>
                                                                            {JSON.stringify(this.state.tugas_dilaporkan_oleh)}
                                                                            <option value={null}>-</option>
                                                                            {this.state.tugas_dilaporkan_oleh.map((d, i) => {
                                                                                return (
                                                                                    <option value={d.id}>{d.nama}</option>
                                                                                );
                                                                            })}
                                                                        </>
                                                                    )}
                                                            </select>
                                                        </div>
                                                     }
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Prioritas</label>
                                                        <select
                                                            value={this.state.prioritas}
                                                            onChange={(e) => {
                                                                this.setState({ prioritas: e.target.value });
                                                            }}
                                                            style={{ height: 36 }}
                                                            className="form-control input-default form-control-sm"
                                                            type="text"
                                                            placeholder=""
                                                        >
                                                            <option value={null}>-</option>
                                                            <option value="Sangat Rendah">Sangat Rendah</option>
                                                            <option value="Rendah">Rendah</option>
                                                            <option value="Sedang">Sedang</option>
                                                            <option value="Tinggi">Tinggi</option>
                                                            <option value="Sangat Tinggi">Sangat Tinggi</option>
                                                        </select>
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Point (0 - 100)</label>
                                                        <input value={this.state.point} onChange={(e) => { this.setState({ point: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" min={0} max={100} placeholder="" />
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Estimasi Awal (Jam)</label>
                                                        <input value={this.state.estimasi_awal} onChange={(e) => { this.setState({ estimasi_awal: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" min={0} max={100} placeholder="" />
                                                    </div>
                                                    <div className="mb-3 col-md-6">
                                                        <label className="form-label">Estimasi Dikerjakan (Jam)</label>
                                                        <input value={this.state.estimasi_dikerjakan} onChange={(e) => { this.setState({ estimasi_dikerjakan: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="number" min={0} max={100} placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="/task-proyek" style={{ marginRight: '10px' }} className="btn btn-xs btn-info">Kembali</a>
                                            <button className="btn btn-xs btn-success" onClick={() => { this.handleSave() }}>Simpan Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <card className="card">
                                <div className="card-body">
                                    <h5 className='text-primary d-inline'>Comments</h5>

                                    {this.state.history.map(p => {
                                        return <div className='media pt-3 pb-3'>
                                            <img width={50} src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEVVYIDn7O3///9KVnlTXn/q7+9NWXva4ONRXH7t8vJMWHvp7u9FUna+xM1JVXlibIng4udZZIP09feTmazc3uRrdJBeaIa2usbGydNye5SAh57t7vH4+frV2N+6vsqnrryJkaWhprZ8hJunrLuQlqrEytKZoLHL0dZueJKEjaHT2d6zE6BNAAAMeElEQVR4nO2de5eCOA+HK5RargJeUMdRRx1v3/8DLqCOKNcmQdg9+zvv2T3v/qE+0zRJ2zRlWttahf7JjX4Oy8V0NAsYY8FsNF0sDz+Re/LDVevfz1r87NCf/2zPzHF0yxKSc844SxT/k3MpLEt3nOC83c/9sMVf0Rah744XgafHYKxaMaruBYux67f0S9og9KMls3RRx/bCKXQrWEZtUFIThvMxcyypAPeUtBw2nlNbLCnh13rJdQGie0jocrn+ovxRhITzHddhg/c2lDrfuXQ+lopwcvBI8B6Q+uGb6JeREIbR1Kl1mmri0plGJFOSgNA/Mp0W7w6psyOBc0UTTpYC51uqJMRy0jHh94LaPF8VG+sCOSFRhN87h867lEI6OxQjgtC/ACO7qqS+RMxHMGE49j7DlzJ6B7BfhRJGVnv+pUjC2nyU8Huqf5QvkT6FTUcI4erQSvyrE9cPkFwOQHj6sIE+JeTpA4Th2OmIL5Gj7nFUCb9HXQ3gTSKYt0v408kMzIp7Py0Sfi0+70Lz0s9KK2QVwhP/XIyvkuQqlqpAuO/cQh/i+r4NwktvABPECznh17RbH/ouMWo6GRsSTmb9mIJPyaDh2rgZ4Ulpe/cz4rKZv2lEOO8yjSmXs6YijJz+jWAqJ6Ih3Hs9BYyDf4NFYz0hLWByxkb4aV59YKwl3BPMweSwUNclC4LZaDSaBUGyqW3Vn7w1kFObpdYRbjzkT5DCY+fLceOertfh0B8MBv5weL2e3M3xcmYeGrN2FGsII0wiw7lwgm10HQ5M0zBsO/7fXcn/MUxzMLxG25kjMJbL9Rp3U024RnhRLuR5M4nZbHtQphjUNK+bs0TEW+64cEJEHOTW6GcYj1wp3FPxaF5/RhaYkTuVW1RVhBNwKsq9szswm+DdIc3B+gz32bIqgasg/AqgXykCN55qjflSezUMd2YBv48HFWl4BeEImGxLubebD19mII29hH7lFEJ4AdqoOF9NAF8i83oGDqNVvl4sJdwDt2T0wwAygPdhHGyhX1uav5URzmHzPk6jTLUJ+CrbBO6VcK9sLVVC+AVLNbi1gVroQ+YGFje4LPE2JYRT2JTHA6aIoO8u8zbFhEfYbLCOeMAYcQxD1IuT8ELCOSzdlju4j8nINhYwC/IKc5siwhAY6uWQhHBgDGGEfFR0bFNEeIBFQj2isNFEZgSbJWLcjPAEy7f5AhMmXmWfYVbkFJwv5glXwMzJ+iUk/IXmNvlT4jwh0Eb5gmYS3mQsYINYYKc5wm9g2iRcUsI1MCvWc/40RziFLpnobDSRDfwVPBf33wmBXowJkmD/lDmGDuL7ts0bYQhd1uu/lEYam+kv9LhZhJWEQDcTR/sBsZUOoJtT787mldCH7o7KJe0Qxog7qEPw/ArCJfSUUPzQTsN4Ih7B5nQpJ4RGijjSrmmNNJ6IEXRfilnfpYQ78EGvfqImtE/gP7dclhF+wzeAxZCccAgvHHAmJYTAZVmqFgjhP0buigkniHO0mU9POIP/HMcvJAQ70jhX6hlhdiY+CX342Ug8hi1YaQD/OVz4BYTg+JOqBULM0ak45glDDB/nLRDiTofDHCF0UdFTwucS448QvC7sJ+FznfggRET7XhI+o/6DcIuqzOshoTy8Eq5wxaM9JOT66oXQxRVw95CQ6fMXQviqoreEj7zmRviFLEzqIyFjXxnCNfKWQS8JdTdDiEi6+0t4381ICUNsEXcvCRkP/wjn2Ksw/SS8FS+khND95Z4T3nZOU0LkJ/WVkAUPQh9dBtxTwnQzIyGE70z2nNBa3wmxsaK3hGlawyimYV8JGbsR+mgj7S1hsiHF0OuKPhMmiRsjiIZJB7Y29rwJxvCYEgLLHrKSJ+rjw8HAOBH85RcJYYjYeb2LrhoqK2hlVFZBGBOCz33/xBdtAMaIeOvS/ZgQnXYzrwUbTWT8ov/4+jwm3KPT7im1l/nTCJ1872NC3D5iLDlux0iTohr0bzvEhMAywKdE1I6RxmYKLIh+KnambIV2pZbblpXaa3S6FaxYiF466aQ1e1kZ+HTLCRl+cdhvQp/Bizr+FYT6ibloU+81oeUy/AK/34QR+0Hnt70mFD/sgN7C6DWhHLMlPrvtMyG/MIL8vdeEO4aqUPgXEJ7ZCPsZ/SaM+Wb/7TFkM0awh9FrQjxf/wn/H8N6tbg+xCfNJGNobfq7xk8I8b60z/s0SbTAx0M+Ir4R9JCN32tjbEqQ05Df6noIfrvrqTinITi14OeW9rwJ/vpxXopfWyRtN1o5t9gQ9IOVF4L1YdIO45ce0fylaNYYrw/xa/xE3CVGtM01Ses6sSfYp0nlkQZF2xwAm2O8S0QEe22p+JRwEO3hkRM1hLVcgv3SVNwivBdkjtHHag/p3wR73jdR3se36bpHOj7BucVN8kBmphSR/iFnxVZEH0WYu5kXuqbFwYrg/PAui+qirO3TGWlyfog/A76LrKuCEdE11k7PgNHn+HfxGZGZQpvTFMlKzvGBTaHyItrNoPQzt1oMfD3NXXJHYqYGoZ+51dMQ1ETd5VAUtxlXyhcmZiFRXdtNJL7GpPJ8iW51bRS1iQ/hMzdjSJawsb/aRIJNybsImgqSDmF6fy2pESYbQ3zAsK+kbzDca4QJ6rwfQg8iqSO9XbigqdV/fiRuEA1on7Zi/dXq42ur/oTsxGMSpjMsc9+CaonIkoUwJiaaEaUjzdyZ0chifjyIW/gg2sCel2XiAd3dtYwEvH2iuaV9refWHON2/5DQOPgU6mwMl/g5osz9w5ByfltAZ2MPwT3gS5S5Q6pRRiFuXUGDaC6JhzB7D1hzKX0YrLLdRL8V8q6Xu9zY+/ivggRFihsy78rex6dMaxI7VT7ZN4b4s+g3vfZUILhWkhVnqv7U3pEP4VtfDI00HwSs9smHkFnaKyFl0IcQEpzYv+qvyeeDENOOLq8eEOZ6DOH6ROU+vnPCfJ8odHuTF3VP6K1zhNBm+oXqnjDI92vTaA70b+qcUDxfgngSfv2HCLlV1DeRMv3umjDbSjhDSLiZ0TVhSf9SwuS0Y8KyHrSEUb9jwtI+wnQzsVvC8l7Q2gTThjarTgm5NSkl1Kg2u9R3TQmTRrnVygm/aF4XVz+hsckOMRnXq/rqI5sJPyR3qkNIUdF9l3XUqghp6oeEcqGiTZf48+r3LbQ1xY6XvCoTYnpbv8ireaME13r+LsjZBfjVlTfJ8ztQjnCCrz2WE/XCGgPVvvtPb5GikBDvbBzQQTDNjrA45ngKXiVD9mfSx7DSKIpdfc4LcPL/Cdf4Wj8qvpP7kG3v0FuaRW8fF72dd4R/k2DwllG2fUQmHE3fztNW0CRR6tsh4hzfNt0p6qXzxu8fahPQ93BvcVJ4qbqQcbAewRnzb66VEmoAv8atqYt6KPcmw4ymwHil7wtZSt6SVT4osUZRxSvxSox2BLJVuShGKSFU2z3lgm8QLznnGCG2ypnae8Dad/NB5NI6+gQG+pRt2OuR2mqcF0/CCsLmKbgUlwkpX6rEVlUY1d/l1rRDo/UM93ZYB1rGOFg3n49iW8pRTqgt6g2V66Nfu62b3ArzsezF6hrCcFS3kBKziN4+M7INs9F85LOiUF9PqPmVOTgXwZ7QgZaoSezg0q+gqCKs3CKW3nHY6gD+MdbZKi/KtxsSlj/vLPXLZ/hSRns9K7dV7swrGaoJS6pQuGjLgZYxmqWxg+vraoQawsKwqJ8pMlBFxrLYkdt5UiXUondDtVjUXoCoZiyYj05ppG9MqL1WJgu274RvUJjLca8WsAFhtkpDSOIMVFFx7DhnGHmtiTYj1ObOY1Jvr13ypYzJfHwAOjVOpjFhHDSSv5sYnbrmuzFGt8v6dWFChVCbMMnE0ehoAr7JNgfb2FS5rAz0ioTa10hSd75AyDbXgTWrStXUCbWwpa7kQJnXZUWyDSLUtP4MYSKz8e9uTqiFXVNl1HQA1Qi1Vddcf1op/GoVQk3rx1y0lX6zGmEvLFXBQgGE2qrrmG+rWCiEsGuf2tyHwgk7dTiqAwgj7G4Y1QcQStjNbFSegRjCLpyqogtFE36aEWSgSMJPTkcTZqBoQm31GUYDwYckjBnbz+OADoaKsPVxxNgnEaHW5nzE89EQxn61jfhoQ+PDq2gIWzBWiuFLRUWokULivOerCAk1Ikiy0buJllDDQtrEeFoLhImAlGZIjqe1RBhrtTIVqsDseOzaoEvUFmGq1Sqs44zZwtbgUrVKeNcqJg1N07DtFDf5l2GaCVmraHf9A3HEDN2tpOABAAAAAElFTkSuQmCC' className='me-3 rounded' />
                                            <div className='media-body'>
                                                <h5 className='m-b-5'>{p.dibuat_oleh}</h5>
                                                <p>{moment(p.dibuat_tgl).format('DD/MM/YYYY hh:mm:ss')}</p>
                                                <div className='mb-0' dangerouslySetInnerHTML={{__html: p.keterangan}} />
                                                {/* <p className='mb-0'>{}</p> */}
                                            </div>
                                        </div>
                                    })}

                                    <div className="mb-3 col-md-8">
                                        <div className="mb-3 col-md-12">
                                            <label className="form-label"></label>
                                            <div id="summernote-comment"></div>
                                        </div>
                                        <button onClick={this.handlePostComment} className='submit btn btn-primary'>Post Comment</button>
                                    </div>
                                </div>
                            </card>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    )
}

export default Page
