import constant from "../../components/constant";
import http from "../../components/http.service";
import React from "react";
import Head from "../../components/Head";
import Navbar from "../app/navbar";
import Sidebar from "../app/sidebar-user";
import Swal from "sweetalert2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ArcElement,
} from "chart.js";
import { Bar, Pie } from "react-chartjs-2";
import { faker } from "@faker-js/faker";
import randomRgba from "random-rgba";
import moment from "moment";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ArcElement
);

export async function getServerSideProps() {
  // Fetch data
  var data = {};
  return { props: { data } };
}

class PageComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statusAbsen: {},
      data: {
        dataPie1: {
          labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
          datasets: [
            {
              label: "# of Votes",
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                "rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(255, 206, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(255, 159, 64, 0.2)",
              ],
              borderColor: [
                "rgba(255, 99, 132, 1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(75, 192, 192, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)",
              ],
              borderWidth: 1,
            },
          ],
        },
        dataPie2: {
          labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
          datasets: [
            {
              label: "# of Votes",
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                "rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(255, 206, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(255, 159, 64, 0.2)",
              ],
              borderColor: [
                "rgba(255, 99, 132, 1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(75, 192, 192, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)",
              ],
              borderWidth: 1,
            },
          ],
        },
      },
    };
  }

  async checkIn() {
    var token = localStorage.getItem("mts-token");
    var getData = await http.post(
      "http://localhost:7070/absen/checkin",
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        }
      }
    );

    this.statusAbsen()
  }

  async checkOut() {
    var token = localStorage.getItem("mts-token");
    var getData = await http.post(
      "http://localhost:7070/absen/checkout",
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        }
      }
    );

    this.statusAbsen()
  }

  async statusAbsen() {
    var token = localStorage.getItem("mts-token");
    var getData = await http.get("http://localhost:7070/absen/status-absen", {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });
    this.setState({ statusAbsen: getData.data });
  }

  async initData(params) {
    if (!params) params = {};
    var token = localStorage.getItem("mts-token");
    if (params && params.page) {
      params.page = this.state.pageNo;
    } else {
      params.page = this.state.pageNo;
    }
    try {
      var getData = await http.get(
        "http://localhost:7070/proyek/dashboard/admin",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: params,
        }
      );
      var backgroundColor = [
        "#F44336",
        "#FFEB3B",
        "#43A047",
        "#1E88E5",
        "#607D8B",
        "#795548",
        "#F4511E",
        "#3949AB",
        "#673AB7",
        "#E91E63",
      ];

      getData.data.taskPerTipe.label.map((d, index) => {
        if (index > 8) {
          backgroundColor.push(randomRgba());
        }
      });
      getData.data.dataPie1 = {
        labels: getData.data.taskPerTipe.label,
        datasets: [
          {
            label: "# of Votes",
            data: getData.data.taskPerTipe.data,
            // backgroundColor: backgroundColor,
            backgroundColor: backgroundColor,
            borderColor: backgroundColor,
            borderWidth: 1,
          },
        ],
      };

      (getData.data.dataPie2 = {
        labels: getData.data.taskPerStatus.data
          ? getData.data.taskPerStatus.label
          : [],
        datasets: [
          {
            label: "# of Votes",
            data: getData.data.taskPerStatus.data
              ? getData.data.taskPerStatus.data
              : [],
            backgroundColor: backgroundColor,
            borderColor: backgroundColor,
            borderWidth: 1,
          },
        ],
      }),
        this.setState({
          data: getData.data,
        });
    } catch (e) {
      console.log(e);
    }
  }

  async componentDidMount() {
    var token = localStorage.getItem("mts-token");
    try {
      let res = await http.get("http://localhost:7070/profile", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      await this.statusAbsen();
      // window.location.href="/app"
    } catch (e) {
      console.log(e)
      // window.location.href = "/login";
    }
    this.initData();
  }

  async navigate(item) {
    setTimeout(() => {
      localStorage.setItem("mts-proyek", JSON.stringify(item));
    }, 500);
    setTimeout(() => {
      window.location.href = `timeline`;
    }, 800);
  }

  render() {
    // var dataPie1 = {
    //     labels: [],
    //     datasets: [
    //         {
    //             label: '# of Votes',
    //             data: [12, 19, 3, 5, 2, 3],
    //             // backgroundColor: [
    //             //     'rgba(255, 99, 132, 0.2)',
    //             //     'rgba(54, 162, 235, 0.2)',
    //             //     'rgba(255, 206, 86, 0.2)',
    //             //     'rgba(75, 192, 192, 0.2)',
    //             //     'rgba(153, 102, 255, 0.2)',
    //             //     'rgba(255, 159, 64, 0.2)',
    //             // ],
    //             // borderColor: [
    //             //     'rgba(255, 99, 132, 1)',
    //             //     'rgba(54, 162, 235, 1)',
    //             //     'rgba(255, 206, 86, 1)',
    //             //     'rgba(75, 192, 192, 1)',
    //             //     'rgba(153, 102, 255, 1)',
    //             //     'rgba(255, 159, 64, 1)',
    //             // ],
    //             borderWidth: 1,
    //         },
    //     ],
    // };
    // try {
    //     dataPie1.label = this.state.data.taskPerTipe.label
    // } catch(e){}
    // var dataPie2 = {
    //     labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    //     datasets: [
    //         {
    //             label: '# of Votes',
    //             data: [12, 19, 3, 5, 2, 3],
    //             // backgroundColor: [
    //             //     'rgba(255, 99, 132, 0.2)',
    //             //     'rgba(54, 162, 235, 0.2)',
    //             //     'rgba(255, 206, 86, 0.2)',
    //             //     'rgba(75, 192, 192, 0.2)',
    //             //     'rgba(153, 102, 255, 0.2)',
    //             //     'rgba(255, 159, 64, 0.2)',
    //             // ],
    //             // borderColor: [
    //             //     'rgba(255, 99, 132, 1)',
    //             //     'rgba(54, 162, 235, 1)',
    //             //     'rgba(255, 206, 86, 1)',
    //             //     'rgba(75, 192, 192, 1)',
    //             //     'rgba(153, 102, 255, 1)',
    //             //     'rgba(255, 159, 64, 1)',
    //             // ],
    //             borderWidth: 1,
    //         },
    //     ],
    // };
    return (
      <>
        <div id="main-wrapper">
          <Navbar pageTitle={this.state.pageTitle} />
          <Sidebar />
          <div className="content-body">
            <div className="container-fluid">
              <div className="">
                <div class="row">
                  <div class="col-xl-3 col-sm-6 m-t35">
                    <div class="card card-coin">
                      <div class="card-body text-center">
                        <h2 class="text-black mb-2 font-w600">
                          {this.state.data.jumlahProyekBerjalan}
                        </h2>
                        <p class="mb-0 fs-14">Jumlah Proyek Berjalan</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-3 col-sm-6 m-t35">
                    <div class="card card-coin">
                      <div class="card-body text-center">
                        <h2 class="text-black mb-2 font-w600">
                          {this.state.data.jumlahProyekSelesai}
                        </h2>
                        <p class="mb-0 fs-14">Jumlah Proyek Selesai</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-3 col-sm-6 m-t35">
                    <div class="card card-coin">
                      <div class="card-body text-center">
                        <h2 class="text-black mb-2 font-w600">
                          {this.state.data.jumlahTask}
                        </h2>
                        <p class="mb-0 fs-14">Jumlah Seluruh Task</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-3 col-sm-6 m-t35">
                    <div class="card card-coin">
                      <div class="card-body text-center">
                        <h2 class="text-black mb-2 font-w600">
                          {this.state.data.jumlahUser}
                        </h2>
                        <p class="mb-0 fs-14">Jumlah Karyawan / Pengguna</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-4" style={{ minHeight: "600px" }}>
                    <div class="card">
                      <div class="card-header pb-2 d-block d-sm-flex flex-wrap border-0">
                        <div class="mb-3">
                          <h4 class="fs-20 text-black">Absen</h4>
                          {/* <p class="mb-0 fs-12">Daftar Proyek yang telah didaftarkan</p> */}
                        </div>
                      </div>
                      <div class="card-body tab-content p-0">
                        <div class="tab-pane active show fade" id="monthly">
                          <div class="table-responsive">
                            <table class="table shadow-hover card-table border-no tbl-btn">
                              <tbody>
                                <tr>
                                  <td style={{ width: "1000px" }}>Masuk</td>
                                  <td style={{ width: "1000px" }}>{this.state.statusAbsen.checkin?moment(this.state.statusAbsen.checkin).format('YYYY-MM-DD HH:mm:ss'):''}</td>
                                </tr>
                                <tr>
                                  <td style={{ width: "1000px" }}>Keluar</td>
                                  <td style={{ width: "1000px" }}>{this.state.statusAbsen.checkout?moment(this.state.statusAbsen.checkout).format('YYYY-MM-DD HH:mm:ss'):''}</td>
                                </tr>
                                <tr>
                                  <td style={{ width: "1000px" }}>Terlambat</td>
                                  <td style={{ width: "1000px" }}>{this.state.statusAbsen.terlambat}</td>
                                </tr>
                                <tr>
                                  <td style={{ width: "1000px" }}>Lembur</td>
                                  <td style={{ width: "1000px" }}>{this.state.statusAbsen.lembur}</td>
                                </tr>
                                <tr>
                                  <td colSpan={2} style={{ width: "1000px" }}>
                                    <a
                                      class="btn btn-block btn-outline-light  float-right"
                                      href="javascript:void(0);"
                                      onClick={()=> this.checkIn()}
                                    >
                                      CHECK IN
                                    </a>
                                  </td>
                                </tr>
                                <tr>
                                  <td colSpan={2} style={{ width: "1000px" }}>
                                    <a
                                      class="btn btn-block btn-outline-light float-right"
                                      href="javascript:void(0);"
                                      onClick={()=> this.checkOut()}
                                    >
                                      CHECK OUT
                                    </a>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="card-footer border-0 p-0 caret mt-1">
                        <a href="/proyek" class="btn-link">
                          <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </a>
                      </div>
                    </div>
                  </div>

                  <div class="col-xl-4" style={{ minHeight: "600px" }}>
                    <div class="card">
                      <div class="card-header pb-2 d-block d-sm-flex flex-wrap border-0">
                        <div class="mb-3">
                          <h4 class="fs-20 text-black">Daftar Proyek</h4>
                          <p class="mb-0 fs-12">
                            Daftar Proyek yang telah didaftarkan
                          </p>
                        </div>
                      </div>
                      <div class="card-body tab-content p-0">
                        <div class="tab-pane active show fade" id="monthly">
                          <div class="table-responsive">
                            <table class="table shadow-hover card-table border-no tbl-btn">
                              <tbody>
                                {this.state.data.listProyek &&
                                  this.state.data.listProyek.length > 0 && (
                                    <>
                                      {this.state.data.listProyek.map((d) => {
                                        return (
                                          <tr
                                            onClick={() => {
                                              this.navigate(d);
                                            }}
                                          >
                                            <td style={{ width: "1000px" }}>
                                              <i
                                                className="fa fa-file"
                                                style={{
                                                  width: "24px",
                                                  height: "24px",
                                                }}
                                              />
                                              <span class="font-w600 text-black">
                                                {d.nama}
                                              </span>
                                            </td>
                                            {d.status == "DRAFT" && (
                                              <td>
                                                <a
                                                  class="btn btn-outline-light float-right"
                                                  href="javascript:void(0);"
                                                >
                                                  {d.status}
                                                </a>
                                              </td>
                                            )}
                                            {d.status == "HOLD" && (
                                              <td>
                                                <a
                                                  class="btn btn-outline-warning float-right"
                                                  href="javascript:void(0);"
                                                >
                                                  {d.status}
                                                </a>
                                              </td>
                                            )}
                                            {d.status == "BERJALAN" && (
                                              <td>
                                                <a
                                                  class="btn btn-outline-info float-right"
                                                  href="javascript:void(0);"
                                                >
                                                  {d.status}
                                                </a>
                                              </td>
                                            )}
                                            {d.status == "GAGAL" && (
                                              <td>
                                                <a
                                                  class="btn btn-outline-danger float-right"
                                                  href="javascript:void(0);"
                                                >
                                                  {d.status}
                                                </a>
                                              </td>
                                            )}
                                            {d.status == "SELESAI" && (
                                              <td>
                                                <a
                                                  class="btn btn-outline-success float-right"
                                                  href="javascript:void(0);"
                                                >
                                                  {d.status}
                                                </a>
                                              </td>
                                            )}
                                          </tr>
                                        );
                                      })}
                                    </>
                                  )}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="card-footer border-0 p-0 caret mt-1">
                        <a href="/proyek" class="btn-link">
                          <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-4" style={{ minHeight: "600px" }}>
                    <div class="card">
                      <div class="card-header pb-2 d-block d-sm-flex flex-wrap border-0">
                        <div class="mb-3">
                          <h4 class="fs-20 text-black">
                            Daftar Tugas / Issue berdasarkan Tipe Tugas
                          </h4>
                          <p class="mb-0 fs-12">
                            Menampilkan visual data tugas / issue berdasarkan
                            tipe tugas seluruh proyek
                          </p>
                        </div>
                      </div>
                      {this.state.data.dataPie1 && (
                        <Pie
                          style={{ padding: "32px", height: "600px" }}
                          data={this.state.data.dataPie1}
                        />
                      )}
                    </div>
                  </div>
                  <div class="col-xl-4" style={{ minHeight: "600px" }}>
                    <div class="card">
                      <div class="card-header pb-2 d-block d-sm-flex flex-wrap border-0">
                        <div class="mb-3">
                          <h4 class="fs-20 text-black">
                            Daftar Tugas / Issue berdasarkan Status
                          </h4>
                          <p class="mb-0 fs-12">
                            Menampilkan visual data tugas / issue berdasarkan
                            status seluruh proyek
                          </p>
                        </div>
                      </div>
                      {this.state.data.dataPie2 && (
                        <Pie
                          style={{ padding: "32px", height: "600px" }}
                          data={this.state.data.dataPie2}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

function Page({ data }) {
  return (
    <>
      <Head />
      <PageComponent data={data} />
    </>
  );
}

export default Page;
