import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from '../app/navbar'
import Sidebar from '../app/sidebar-proyek'
import Swal from 'sweetalert2'
import Router from 'next/router'

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class PageComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "Formulir Timeline",
            id: null,
            nama: null,
            keterangan: null,
            status: 'DRAFT',
            timeline_dimulai: null,
            timeline_berakhir: null,
            proyek_id: null,
            errorMessage: [],
            formIsError: false
        }
    }

    async initData() {
        var query = Router.query
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get(`http://localhost:7070/timeline/${query.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            this.setState({
                nama: res.data.nama,
                keterangan: res.data.keterangan,
                status: res.data.status,
                timeline_dimulai: res.data.timeline_dimulai,
                timeline_berakhir: res.data.timeline_berakhir,
                proyek_id: res.data.proyek_id,
                id: res.data.id
            })
        } catch(e){}
    }

    async componentDidMount() {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        this.initData()
    }

    async handleSave() {
        var errorMessage = ""
        if (!this.state.nama) {
            errorMessage += "nama diperlukan,"
        }
        if (!this.state.keterangan) {
            errorMessage += " keterangan diperlukan,"
        }
        if (!this.state.status) {
            errorMessage += " status diperlukan,"
        }
        if (!this.state.timeline_dimulai) {
            errorMessage += " timeline dimulai diperlukan,"
        }
        if (!this.state.timeline_berakhir) {
            errorMessage += " timeline berakhir diperlukan,"
        }
        if (errorMessage == "") {
            var fromData = {
                nama: this.state.nama,
                keterangan: this.state.keterangan,
                status: this.state.status,
                timeline_dimulai: this.state.timeline_dimulai,
                timeline_berakhir: this.state.timeline_berakhir,
                id: this.state.id,
                proyek_id: this.state.proyek_id ? this.state.proyek_id : JSON.parse(localStorage.getItem("mts-proyek")).id
            }
            try {
                var token = localStorage.getItem('mts-token')
                if(this.state.id){
                    let res = await http.put(`http://localhost:7070/timeline/${this.state.id}`, fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                } else {
                    let res = await http.post('http://localhost:7070/timeline', fromData, {
                        headers: {
                            'Authorization': `Bearer ${token}`
                        }
                    })
                }
                Swal.fire('Sukses', 'Data berhasil disimpan', 'success')
                setTimeout(()=> {
                    window.location.href="/timeline"
                }, 1000)
            } catch(e){
                Swal.fire('Gagal', `Data tidak berhasil disimpan dengan pesan error ${e.message}`, 'error')
            }
        } else {
            this.setState({
                formIsError: true,
                errorMessage: errorMessage
            })
        }
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    <Sidebar />
                    <div className="content-body">
                        <div className='container-fluid'>
                            <div className="card">
                                <div className="card-body">
                                    <div className="basic-form">
                                        <div className='mb3'>
                                            <div className="row">
                                                <div className='col-md-12'>
                                                    {this.state.formIsError &&
                                                        <div className="alert alert-danger solid alert-dismissible fade show">
                                                            <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" className="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                            {/* <strong>Error!</strong>  */}
                                                            {this.state.errorMessage}
                                                            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                                            </button>
                                                        </div>
                                                    }
                                                </div>
                                                <div className="mb-3 col-md-12">
                                                    <label className="form-label">Nama</label>
                                                    <input value={this.state.nama} onChange={(e) => { this.setState({ nama: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                </div>
                                                <div className="mb-3 col-md-12">
                                                    <label className="form-label">Keterangan</label>
                                                    <input value={this.state.keterangan} onChange={(e) => { this.setState({ keterangan: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                </div>
                                                {/* <div className="mb-3 col-md-12">
                                                    <label className="form-label">Status</label>
                                                    <select value={this.state.status} onChange={(e) => { this.setState({ status: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="">
                                                        <option value={null}>-</option>
                                                        <option value="DRAFT">DRAFT</option>
                                                        <option value="IN-PROGRESS">IN-PROGRESS</option>
                                                        <option value="DONE">DONE</option>
                                                    </select>
                                                </div> */}
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Timeline dimulai</label>
                                                    <input value={this.state.timeline_dimulai} onChange={(e) => { this.setState({ timeline_dimulai: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="date" placeholder="" />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Timeline berakhir</label>
                                                    <input value={this.state.timeline_berakhir } onChange={(e) => { this.setState({  timeline_berakhir : e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="date" placeholder="" />
                                                </div>
                                            </div>
                                            <a href="/timeline" style={{ marginRight: '10px' }} className="btn btn-xs btn-info">Kembali</a>
                                            <button className="btn btn-xs btn-success" onClick={() => { this.handleSave() }}>Simpan Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    )
}

export default Page
