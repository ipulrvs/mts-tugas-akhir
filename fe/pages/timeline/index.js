import constant from '../../components/constant'
import http from '../../components/http.service'
import React from 'react'
import Head from '../../components/Head'
import Navbar from '../app/navbar'
import Sidebar from '../app/sidebar-proyek'
import SidebarUserProyek from '../app/sidebar-proyek-user'
import Swal from 'sweetalert2'
import PropTypes from 'prop-types'; // ES6
import Timeline, {
    TimelineHeaders,
    SidebarHeader,
    DateHeader
  } from 'react-calendar-timeline'
import 'react-calendar-timeline/lib/Timeline.css'
import moment from 'moment'

export async function getServerSideProps() {
    // Fetch data
    var data = {}
    return { props: { data } }
}

class PageComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageTitle: "Timeline",
            data: [],
            total: [],
            limit: 20,
            totalPage: 0,
            pageNo: 0,
            pageRangeNo: 0,
            pageRange: [],
            showSearch: false,
            nama: null,
            keterangan: null,
            timelineGroups: [
                { id: 1, title: 'group 1' }, 
                { id: 2, title: 'group 2' }, 
                { id: 3, title: 'group 3' },
                { id: 4, title: 'group 4' },
                { id: 5, title: 'group 5' },
                { id: 6, title: 'group 6' },
                { id: 7, title: 'group 7' },
                { id: 8, title: 'group 8' },
                { id: 9, title: 'group 9' },
                { id: 10, title: 'group 10' },
                { id: 11, title: 'group 11' },
                { id: 12, title: 'group 12' },
                { id: 13, title: 'group 13' },
                { id: 14, title: 'group 14' },
                { id: 15, title: 'group 15' },
                { id: 16, title: 'group 16' },
                { id: 17, title: 'group 17' },
                { id: 18, title: 'group 18' },
                { id: 19, title: 'group 19' },
                { id: 20, title: 'group 20' },
            ],
            timelineItems: [
                { canChangeGroup: false, id: 1, group: 1, title: 'group 1' }, 
                { canChangeGroup: false, id: 2, group: 2, title: 'group 2' }, 
                { canChangeGroup: false, id: 3, group: 3, title: 'group 3' },
                { canChangeGroup: false, id: 4, group: 4, title: 'group 4' },
                { canChangeGroup: false, id: 5, group: 5, title: 'group 5' },
                { canChangeGroup: false, id: 6, group: 6, title: 'group 6' },
                { canChangeGroup: false, id: 7, group: 7, title: 'group 7' },
                { canChangeGroup: false, id: 8, group: 8, title: 'group 8' },
                { canChangeGroup: false, id: 9, group: 9, title: 'group 9' },
                { canChangeGroup: false, id: 10,group: 10, title: 'group 10' },
                { canChangeGroup: false, id: 11,group: 11, title: 'group 11' },
                { canChangeGroup: false, id: 12,group: 12, title: 'group 12' },
                { canChangeGroup: false, id: 13,group: 13, title: 'group 13' },
                { canChangeGroup: false, id: 14,group: 14, title: 'group 14' },
                { canChangeGroup: false, id: 15,group: 15, title: 'group 15' },
                { canChangeGroup: false, id: 16,group: 16, title: 'group 16' },
                { canChangeGroup: false, id: 17,group: 17, title: 'group 17' },
                { canChangeGroup: false, id: 18,group: 18, title: 'group 18' },
                { canChangeGroup: false, id: 19,group: 19, title: 'group 19' },
                { canChangeGroup: false, id: 20,group: 20, title: 'group 20' },
            ],
            isUser: false
        }
    }

    async initData(params) {
        try {
            var user = JSON.parse(localStorage.getItem('mts-user'))
            if (user.role.nama == "User") {
                this.setState({ isUser: true })
            }
        } catch (e) { }
        if (!params) params = {}
        var token = localStorage.getItem('mts-token')
        if (params && params.page) {
            params.page = this.state.pageNo
        } else {
            params.page = this.state.pageNo
        }
        params.proyek_id = JSON.parse(localStorage.getItem("mts-proyek")).id
        try {
            var getData = await http.get('http://localhost:7070/timeline', {
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                params: params
            })


            // Pagination
            var totalData = getData.data.total
            var limitData = this.state.limit
            var totalPage = Math.ceil(totalData / limitData)
            var pageRange = []
            var pageRangeNo = this.state.pageRangeNo
            var pageNo = this.state.pageNo
            var initPage = 0
            while (initPage < totalPage) {
                pageRange.push(initPage)
                initPage++;
            }
            var pageRangeLimit = 5
            var newPageRange = []
            //newPageRange = pageRange.slice((pageRangeNo*pageRangeLimit)*pageNo, (pageRangeNo + 1) * pageRangeLimit)
            console.log(newPageRange, pageRangeNo, "TELL ME")
            // Pagination
            // timeline
            var timelineGroups = []
            var timelineItems = []
            try {
                getData.data.data.map((d)=> {
                    timelineGroups.push({
                        id: d.id,
                        title: d.nama
                    })
                    var finishTask = 0
                    var allTask = 0
                    try {
                        var finishTaskList = d.tasks.filter((d)=> {
                            return d.status.task_selesai == true
                        })
                        finishTask = finishTaskList.length
                        allTask = d.tasks.length
                    } catch(e){}
                    timelineItems.push({
                        canChangeGroup: false,
                        id: d.id,
                        group: d.id,
                        title: `${finishTask}/${allTask}`,
                        // title: `( ${moment(d.timeline_dimulai).format('DD-MM-YYYY')} - ${moment(d.timeline_berakhir).format('DD-MM-YYYY')} )`,
                        start_time: moment(d.timeline_dimulai),
                        end_time: moment(d.timeline_berakhir).add(1, 'day'),
                    },)

                })
            } catch(e){}
            // timeline
            this.setState({
                data: getData.data.data,
                total: getData.data.total,
                showSearch: false,
                pageRangeNo: 0,
                pageRange: newPageRange,
                totalPage: totalPage,
                timelineGroups: timelineGroups,
                timelineItems: timelineItems
            })
        } catch (e) {
            console.log(e)
        }
    }

    async componentDidMount() {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.get('http://localhost:7070/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) {
            window.location.href = "/login"
        }
        this.initData()
    }

    async removeItem(d) {
        var token = localStorage.getItem('mts-token')
        try {
            let res = await http.delete(`http://localhost:7070/timeline/${d.id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            // window.location.href="/app"
        } catch (e) { }
    }

    async edit(d) {
        window.location.href = `/timeline/${d.id}`
    }

    async hapus(data) {
        Swal.fire({
            title: 'Hapus Data',
            text: "Apa anda yakin akan melakukan proses ini ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(async (result) => {
            if (result.isConfirmed) {
                await this.removeItem(data)
                this.initData()
                Swal.fire(
                    'Berhasil',
                    'Data berhasil dihapus.',
                    'success'
                )
            }
        })
    }

    async handlePage(d) {
        this.setState({
            pageNo: d
        }, () => {
            this.initData()
        })
    }

    async handleCari() {
        var nama = this.state.nama
        var keterangan = this.state.keterangan
        var where = {}
        if (nama) where.nama = nama
        if (keterangan) where.keterangan = keterangan
        this.initData({
            where: where
        })
    }

    render() {
        return (
            <>
                <div id="main-wrapper">
                    <Navbar pageTitle={this.state.pageTitle} />
                    {!this.state.isUser &&
                        <Sidebar />
                    }
                    {this.state.isUser &&
                        <SidebarUserProyek />
                    }
                    <div className="content-body">
                        <div className='container-fluid'>
                            {this.state.showSearch &&
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title">Pencarian</h4>
                                    </div>
                                    <div className="card-body">
                                        <div className="basic-form">
                                            <div className="row">
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Nama</label>
                                                    <input value={this.state.nama} onChange={(e) => { this.setState({ nama: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label">Keterangan</label>
                                                    <input value={this.state.keterangan} onChange={(e) => { this.setState({ keterangan: e.target.value }) }} style={{ height: 36 }} className="form-control input-default form-control-sm" type="text" placeholder="" />
                                                </div>
                                            </div>
                                            <button className="btn btn-xs btn-primary" onClick={(e) => this.handleCari()}>Cari Data</button>
                                        </div>
                                    </div>
                                </div>
                            }
                            <div className='card'>
                                <div className="card-header d-sm-flex d-block">
                                    <div className="me-auto mb-sm-0 mb-3">
                                        <h4 className="card-title mb-2">Timeline</h4>
                                        <span>Tampilan tugas yang terukur besar yang dikerjakan satu bulan atau lebih dari satu proyek berjalan, <b>klik 2x module untuk dapat melihat task</b></span>
                                    </div>
                                    {!this.state.isUser &&
                                        <a onClick={() => {
                                            window.location.href = window.location.href + '/new'
                                        }} className="btn btn-xs btn-success">+ Tambah Data</a>
                                    }
                                    <a style={{ marginLeft: 10 }} onClick={() => {
                                        window.location.href = window.location.href
                                    }} className="btn btn-xs btn-info">Reload Data</a>
                                    <a href="javascript:void(0);" style={{ marginLeft: 10 }}
                                        onClick={() => { this.setState({ showSearch: !this.state.showSearch }) }}
                                        className="btn btn-xs btn-info">Pencarian Data</a>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <Timeline
                                            onItemClick={(item, e, time)=> {
                                                var timelineId = item
                                                localStorage.setItem("mts-timeline-id", item)
                                                window.location.href = "/task-timeline"
                                            }}
                                            groups={this.state.timelineGroups}
                                            items={this.state.timelineItems}
                                            defaultTimeStart={moment().add(-20, 'days')}
                                            defaultTimeEnd={moment().add(20, 'days')}
                                        >   
                                            {/* <TimelineHeaders>
                                                <DateHeader unit="primaryHeader" />
                                                <DateHeader />
                                            </TimelineHeaders> */}
                                        </Timeline>
                                        <hr />
                                        <div className="dataTables_info" id="ListDatatableView_info" role="status" aria-live="polite">Menampilkan {this.state.limit * (this.state.pageNo) + 1} dari {this.state.total} data</div>
                                        <nav style={{ float: 'right' }}>
                                            <ul className="pagination pagination-sm pagination-gutter pagination-primary no-bg">
                                                {this.state.pageNo != 0 &&
                                                    <li className="page-item page-indicator">
                                                        <a className="page-link" style={{ cursor: 'pointer' }} onClick={() => {
                                                            this.setState({
                                                                pageNo: this.state.pageNo - 1
                                                            }, () => {
                                                                this.initData()
                                                            })
                                                        }}>
                                                            <i className="la la-angle-left"></i>
                                                        </a>
                                                    </li>
                                                }
                                                {this.state.pageRange && this.state.pageRange.length > 0 &&
                                                    <>
                                                        {this.state.pageRange.map((d, key) => {
                                                            var className = "page-item"
                                                            if (d == this.state.pageNo) className = "page-item active"
                                                            return <li style={{ cursor: "pointer" }} className={className} key={key} onClick={() => this.handlePage(d)}><a className="page-link">{d + 1}</a></li>
                                                        })}
                                                    </>
                                                }
                                                {this.state.pageNo != (this.state.totalPage -1) &&
                                                    <li className="page-item page-indicator">
                                                        <a className="page-link" style={{ cursor: 'pointer' }} onClick={() => {
                                                            this.setState({
                                                                pageNo: this.state.pageNo + 1,
                                                                pageRangeNo : this.state.pageRangeNo + 1
                                                            }, () => {
                                                                this.initData()
                                                            })
                                                        }}>
                                                            <i className="la la-angle-right"></i>
                                                        </a>
                                                    </li>
                                                }
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function Page({ data }) {
    return (
        <>
            <Head />
            <PageComponent data={data} />
        </>
    )
}

export default Page
