import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User_roleService } from './user_role.service';
import { CreateUser_roleDto } from './dto/create-user_role.dto';
import { UpdateUser_roleDto } from './dto/update-user_role.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('user_role')
@Controller('user_role')
export class User_roleController {
  constructor(private readonly user_roleService: User_roleService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createUser_roleDto: CreateUser_roleDto) {
    return this.user_roleService.create(createUser_roleDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.user_roleService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.user_roleService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateUser_roleDto: UpdateUser_roleDto) {
    return this.user_roleService.update(+id, updateUser_roleDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.user_roleService.remove(+id);
  }
}
