import { PartialType } from '@nestjs/mapped-types';
import { CreateUser_roleDto } from './create-user_role.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateUser_roleDto extends PartialType(CreateUser_roleDto) {}
