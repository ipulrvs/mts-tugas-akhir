import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUser_roleDto {
    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    @IsNotEmpty()
    user_id: number

    @IsNotEmpty()
    role_id: number

    @IsNotEmpty()
    keterangan: string
}
