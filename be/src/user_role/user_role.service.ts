import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUser_roleDto } from './dto/create-user_role.dto';
import { UpdateUser_roleDto } from './dto/update-user_role.dto';
import { User_role } from './entities/user_role.entity';

@Injectable()
export class User_roleService {
  constructor(
    @InjectRepository(User_role)
    private user_roleRepository: Repository<User_role>,
  ) { }

  async create(createUser_roleDto: CreateUser_roleDto) {
    var inserted = await this.user_roleRepository.insert(createUser_roleDto);
    return {
      ...createUser_roleDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      where = JSON.parse(where)
    }
    console.log(typeof where, "WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.user_roleRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.user_roleRepository.findOneBy({ id });
  }

  update(id: number, updateUser_roleDto: UpdateUser_roleDto) {
    console.log(updateUser_roleDto)
    return this.user_roleRepository.save(updateUser_roleDto);
  }

  remove(id: number) {
    return this.user_roleRepository.delete(id);
  }
}
