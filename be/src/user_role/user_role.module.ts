import { Module } from '@nestjs/common';
import { User_roleService } from './user_role.service';
import { User_roleController } from './user_role.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { User_role } from './entities/user_role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User_role])],
  controllers: [User_roleController],
  providers: [User_roleService]
})
export class User_roleModule {}
