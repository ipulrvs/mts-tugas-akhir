import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DepartemenModule } from './departemen/departemen.module';
import { JabatanModule } from './jabatan/jabatan.module';
import { ProyekModule } from './proyek/proyek.module';
import { RoleModule } from './role/role.module';
import { MenuModule } from './menu/menu.module';
import { PermissionModule } from './permission/permission.module';
import { Role_permissionModule } from './role_permission/role_permission.module';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { JadwalModule } from './jadwal/jadwal.module';
import { Jadwal_perorangModule } from './jadwal_perorang/jadwal_perorang.module';
import { AbsenModule } from './absen/absen.module';
import { UserModule } from './user/user.module';
import { Tipe_tugasModule } from './tipe_tugas/tipe_tugas.module';
import { KolomModule } from './kolom/kolom.module';
import { MemberModule } from './member/member.module';
import { TimelineModule } from './timeline/timeline.module';
import { SprintModule } from './sprint/sprint.module';
import { TaskModule } from './task/task.module';
import { SettingModule } from './setting/setting.module';
import { Task_historyModule } from './task_history/task_history.module';

@Module({
  imports: [
    TaskModule,
    Task_historyModule,
    SprintModule,
    TimelineModule,
    MemberModule,
    KolomModule,
    Tipe_tugasModule,
    AbsenModule,
    // Jadwal_perorangModule,
    JadwalModule,
    MenuModule,
    RoleModule,
    DepartemenModule,
    JabatanModule,
    ProyekModule,
    PermissionModule,
    Role_permissionModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'mts',
      entities: [__dirname + '/../**/*.entity.js'],
      synchronize: true,
      autoLoadEntities: true,
    }),
    AuthModule,
    UsersModule,
    UserModule,
    SettingModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
