import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateJabatanDto } from './dto/create-jabatan.dto';
import { UpdateJabatanDto } from './dto/update-jabatan.dto';
import { Jabatan } from './entities/jabatan.entity';
import { Like } from "typeorm"

@Injectable()
export class JabatanService {
  constructor(
    @InjectRepository(Jabatan)
    private jabatanRepository: Repository<Jabatan>,
  ) { }

  async create(createJabatanDto: CreateJabatanDto) {
    var inserted = await this.jabatanRepository.insert(createJabatanDto);
    return {
      ...createJabatanDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    var order = {}
    order[sortBy] = sort
    const getData = await this.jabatanRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.jabatanRepository.findOneBy({ id });
  }

  update(id: number, updateJabatanDto: UpdateJabatanDto) {
    console.log(updateJabatanDto)
    return this.jabatanRepository.save(updateJabatanDto);
  }

  remove(id: number) {
    return this.jabatanRepository.delete(id);
  }
}
