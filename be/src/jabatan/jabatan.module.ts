import { Module } from '@nestjs/common';
import { JabatanService } from './jabatan.service';
import { JabatanController } from './jabatan.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Jabatan } from './entities/jabatan.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Jabatan])],
  controllers: [JabatanController],
  providers: [JabatanService]
})
export class JabatanModule {}
