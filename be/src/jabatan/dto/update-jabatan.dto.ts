import { PartialType } from '@nestjs/mapped-types';
import { CreateJabatanDto } from './create-jabatan.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateJabatanDto extends PartialType(CreateJabatanDto) {}
