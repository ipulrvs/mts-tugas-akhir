import { ManyToOne, JoinColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';
import { Jadwal } from 'src/jadwal/entities/jadwal.entity';
import { User } from 'src/user/entities/user.entity';

@Entity()
export class Absen {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'datetime' })
    checkin: string;

    @Column({ type: 'datetime' })
    checkout: string;

    @Column()
    terlambat: number;

    @Column()
    lembur: number;

    @Column()
    tipe_kehadiran: string;

    @Column()
    alasan: string;

    @ManyToOne(() => User, {
        eager: true,
    })
    @JoinColumn()
    user: User

    @ManyToOne(() => Jadwal)
    @JoinColumn()
    jadwal: Jadwal
}
