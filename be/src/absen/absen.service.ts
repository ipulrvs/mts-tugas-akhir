import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Like,
  Repository,
  MoreThanOrEqual,
  LessThanOrEqual,
  Between,
} from 'typeorm';
import { CreateAbsenDto } from './dto/create-absen.dto';
import { UpdateAbsenDto } from './dto/update-absen.dto';
import { Absen } from './entities/absen.entity';
import { User } from '../user/entities/user.entity';
import * as xlsx from 'xlsx';
import * as moment from 'moment';

@Injectable()
export class AbsenService {
  constructor(
    @InjectRepository(Absen)
    private absenRepository: Repository<Absen>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async create(createAbsenDto: CreateAbsenDto) {
    var inserted = await this.absenRepository.insert(createAbsenDto);
    return {
      ...createAbsenDto,
    };
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      where = JSON.parse(where);
    }
    try {
      console.log(typeof where, 'WHERE');
      if (params.userId) {
        params.where = {
          ...params.where,
          user: { id: params.userId },
        };
        delete params.userId;
        console.log(where, params);
      }
      where = params.where;
    } catch (e) {}
    var order = {};
    order[sortBy] = sort;
    console.log(where, 'BEFORE FIND');
    const getData = await this.absenRepository.findAndCount({
      relations: ['user'],
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  findOne(id: number) {
    return this.absenRepository.findOneBy({ id });
  }

  update(id: number, updateAbsenDto: UpdateAbsenDto) {
    console.log(updateAbsenDto);
    return this.absenRepository.save(updateAbsenDto);
  }

  remove(id: number) {
    return this.absenRepository.delete(id);
  }

  async checkin(username: string) {
    var user = await this.userRepository.findOne({
      where: { username: username },
      relations: ['jadwal'],
    });

    if (user) {
      var absentExisting = await this.absenRepository.findOne({
        where: {
          checkin: Between(
            `${moment().format('YYYY-MM-DD') + ' 00:00:00'}`,
            `${moment().format('YYYY-MM-DD') + ' 23:59:59'}`,
          ),
        },
        relations: ['user'],
      });

      // @ts-ignore
      absentExisting = absentExisting ? absentExisting : {};

      var terlambat = moment().diff(
        moment(
          moment().format('YYYY-MM-DD') + ' ' + user.jadwal.jam_masuk,
          'YYYY-MM-DD HH:mm',
        ),
        'minutes',
      );

      absentExisting = {
        ...absentExisting,
        // @ts-ignore
        lembur: 0,
        tipe_kehadiran: '',
        alasan: '',
        user: user,
        jadwal: user.jadwal,
      };

      var absen = {
        ...absentExisting,
        checkin: moment().format('YYYY-MM-DD HH:mm:ss'),
        checkout: moment().format('YYYY-MM-DD HH:mm:ss'),
        terlambat: terlambat > 0 ? terlambat : 0,
      };
      var res = await this.absenRepository.save(absen);

      return res;
    }
  }

  async checkout(username) {
    var user = await this.userRepository.findOne({
      where: { username: username },
      relations: ['jadwal'],
    });

    if (user) {
      var lembur = moment().diff(moment(
        moment().format('YYYY-MM-DD') + ' ' + user.jadwal.jam_keluar,
        'YYYY-MM-DD HH:mm',
      ), 'minutes');

      var absentExisting = await this.absenRepository.findOne({
        where: {
          checkin: Between(
            `${moment().format('YYYY-MM-DD') + ' 00:00:00'}`,
            `${moment().format('YYYY-MM-DD') + ' 23:59:59'}`,
          ),
        },
        relations: ['user'],
      });

      if (!absentExisting) {
        absentExisting = {
          ...absentExisting,
          terlambat: 0,
          tipe_kehadiran: '',
          alasan: '',
          user: user,
          jadwal: user.jadwal,
        };
      }

      var absen = {
        ...absentExisting,
        checkin: absentExisting.checkin
          ? absentExisting.checkin
          : moment().format('YYYY-MM-DD HH:mm:ss'),
        checkout: moment().format('YYYY-MM-DD HH:mm:ss'),
        lembur: lembur > 0 ? lembur : 0,
      };
      var res = await this.absenRepository.save(absen);

      return res;
    }
  }

  async statusAbsen(username) {
    var user = await this.userRepository.findOne({
      where: { username: username },
      relations: ['jadwal'],
    });

    if (user) {
      var absentExisting = await this.absenRepository.findOne({
        where: {
          checkin: Between(
            `${moment().format('YYYY-MM-DD') + ' 00:00:00'}`,
            `${moment().format('YYYY-MM-DD') + ' 23:59:59'}`,
          ),
        },
        relations: ['user'],
      });

      return absentExisting ? absentExisting : {};
    } else {
      return {};
    }
  }

  async report() {
    const getData = await this.absenRepository.find();
    let workbook = xlsx.utils.book_new();
    let worksheet = xlsx.utils.json_to_sheet(
      getData.map((d, i) => ({
        NO: i + 1,
        NAMA: d.user.nama,
        'CHECK IN': d.checkin,
        'CHECK OUT': d.checkout,
        JENIS: d.tipe_kehadiran,
        ALASAN: d.alasan,
      })),
    );
    xlsx.utils.book_append_sheet(workbook, worksheet, 'ABSEN REPORT');
    return xlsx.write(workbook, { type: 'buffer' });
  }
}
