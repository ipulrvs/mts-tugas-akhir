import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards, Res, StreamableFile, Response, Request } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AbsenService } from './absen.service';
import { CreateAbsenDto } from './dto/create-absen.dto';
import { UpdateAbsenDto } from './dto/update-absen.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { request } from 'http';

@ApiTags('absen')
@Controller('absen')
export class AbsenController {
  constructor(private readonly absenService: AbsenService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createAbsenDto: CreateAbsenDto) {
    return this.absenService.create(createAbsenDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.absenService.findAll(params);
  }

  @Get('/report')
  async report(@Res() res) {
    var buffer = await this.absenService.report();
    res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    res.set('Content-Disposition', 'attachment; filename="report-absen.xlsx"')
    res.send(buffer);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/checkin')
  checkin(@Request() req: any) {
    return this.absenService.checkin(req.user.username);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/checkout')
  checkout(@Request() req: any) {
    return this.absenService.checkout(req.user.username);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/status-absen')
  statusAbsen(@Request() req: any) {
    return this.absenService.statusAbsen(req.user.username);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.absenService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateAbsenDto: UpdateAbsenDto) {
    return this.absenService.update(+id, updateAbsenDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.absenService.remove(+id);
  }

  

  // @UseGuards(JwtAuthGuard)
  
}
