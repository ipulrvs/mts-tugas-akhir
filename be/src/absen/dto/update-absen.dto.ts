import { PartialType } from '@nestjs/mapped-types';
import { CreateAbsenDto } from './create-absen.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateAbsenDto extends PartialType(CreateAbsenDto) {}
