import { IsEmail, IsNotEmpty } from 'class-validator';
import { Jadwal } from 'src/jadwal/entities/jadwal.entity';
import { User } from 'src/user/entities/user.entity';

export class CreateAbsenDto {
    checkin: string;

    checkout: string;

    terlambat: number;

    lembur: number;

    tipe_kehadiran: string;

    alasan: string;

    user: User;

    jadwal: Jadwal;
}
