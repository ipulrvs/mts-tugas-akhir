import { Module } from '@nestjs/common';
import { AbsenService } from './absen.service';
import { AbsenController } from './absen.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Absen } from './entities/absen.entity';
import { User } from '../user/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Absen, User])],
  controllers: [AbsenController],
  providers: [AbsenService]
})
export class AbsenModule {}
