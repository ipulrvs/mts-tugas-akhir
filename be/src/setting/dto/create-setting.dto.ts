import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateSettingDto {
  @IsNotEmpty()
  ikon: string;

  @IsNotEmpty()
  nama: string;

  warnaTopbar: string;

  warnaSidebar: string;
}
