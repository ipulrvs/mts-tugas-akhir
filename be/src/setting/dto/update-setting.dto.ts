import { CreateSettingDto } from './create-setting.dto';
import { PartialType } from '@nestjs/mapped-types';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateSettingDto extends PartialType(CreateSettingDto) {}
