import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Like } from 'typeorm';
import { CreateSettingDto } from './dto/create-setting.dto';
import { UpdateSettingDto } from './dto/update-setting.dto';
import { Setting } from './entities/setting.entity';

@Injectable()
export class SettingService {
  constructor(
    @InjectRepository(Setting)
    private settingRepository: Repository<Setting>,
  ) {}

  async create(createSettingDto: CreateSettingDto) {
    var existing = await this.settingRepository.findOneBy({ id: 1 });

    if (existing) {
      createSettingDto = {
        ...existing,
        ...createSettingDto,
      };
      await this.settingRepository.save(createSettingDto);
    } else {
      await this.settingRepository.insert(createSettingDto);
    }
    return {
      ...createSettingDto,
    };
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 20;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      try {
        where = JSON.parse(where);
        if (where.nama) where.nama = Like(`%${where.nama}%`);
        if (where.keterangan) where.keterangan = Like(`%${where.keterangan}%`);
      } catch (e) {}
    }
    var order = {};
    order[sortBy] = sort;
    const getData = await this.settingRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  findOne(id: number) {
    return this.settingRepository.findOneBy({ id });
  }

  update(id: number, updateSettingDto: UpdateSettingDto) {
    console.log(updateSettingDto);
    return this.settingRepository.save(updateSettingDto);
  }

  remove(id: number) {
    return this.settingRepository.delete(id);
  }
}
