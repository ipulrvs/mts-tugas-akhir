import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Setting {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('longtext')
  ikon: string;

  @Column()
  nama: string;

  @Column({ name: 'warna_topbar' })
  warnaTopbar: string;

  @Column({ name: 'warna_sidebar' })
  warnaSidebar: string;
}
