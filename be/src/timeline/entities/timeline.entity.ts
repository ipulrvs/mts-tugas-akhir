import { OneToMany, JoinColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';
import { Task } from 'src/task/entities/task.entity';

@Entity()
export class Timeline {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nama: string;

    @Column()
    keterangan: string;

    @Column({type: 'date'})
    timeline_dimulai: Date

    @Column({type: 'date'})
    timeline_berakhir: Date

    @Column()
    status: string;

    @OneToMany(() => Task, (task) => task.timeline, {
        eager: true,
    })
    @JoinColumn()
    tasks: Task[]

    @Column()
    dibuat_oleh: string

    @CreateDateColumn()
    dibuat_tgl: Date

    @Column()
    diupdate_oleh: string

    @UpdateDateColumn()
    diupdate_tgl: Date

    @Column()
    user_id: number

    @Column()
    proyek_id: number
}

