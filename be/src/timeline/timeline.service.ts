import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTimelineDto } from './dto/create-timeline.dto';
import { UpdateTimelineDto } from './dto/update-timeline.dto';
import { Timeline } from './entities/timeline.entity';
import { Like } from "typeorm"

@Injectable()
export class TimelineService {
  constructor(
    @InjectRepository(Timeline)
    private timelineRepository: Repository<Timeline>,
  ) { }

  async create(createTimelineDto: CreateTimelineDto) {
    var inserted = await this.timelineRepository.insert(createTimelineDto);
    return {
      ...createTimelineDto
    }
  }

  async findAll(params): Promise<any> {
    console.log(params, "TELL ME")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    where.proyek_id = params.proyek_id
    var order = {}
    order[sortBy] = sort
    const getData = await this.timelineRepository.findAndCount({
      relations: ['tasks'],
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    try {
      var data = getData[0]
      
    } catch(e){}
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.timelineRepository.findOneBy({ id });
  }

  update(id: number, updateTimelineDto: UpdateTimelineDto) {
    console.log(updateTimelineDto)
    return this.timelineRepository.save(updateTimelineDto);
  }

  remove(id: number) {
    return this.timelineRepository.delete(id);
  }
}
