import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateTimelineDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    @IsNotEmpty()
    timeline_dimulai: Date

    @IsNotEmpty()
    timeline_berakhir: Date

    @IsNotEmpty()
    status: string

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number

    proyek_id: number
}
