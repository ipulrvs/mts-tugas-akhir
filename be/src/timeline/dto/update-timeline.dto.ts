import { PartialType } from '@nestjs/mapped-types';
import { CreateTimelineDto } from './create-timeline.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateTimelineDto extends PartialType(CreateTimelineDto) {}
