import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Task_historyService } from './task_history.service';
import { CreateTask_historyDto } from './dto/create-task_history.dto';
import { UpdateTask_historyDto } from './dto/update-task_history.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('task_history')
@Controller('task_history')
export class Task_historyController {
  constructor(private readonly task_historyService: Task_historyService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createTask_historyDto: CreateTask_historyDto) {
    return this.task_historyService.create(createTask_historyDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    console.log(params)
    try {
      return this.task_historyService.findAll(params);
    } catch (error) {
      console.log(error, 'asdsa')
    }
    
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  allData(@Query() params) {
    return this.task_historyService.allData(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/timeline')
  timeline(@Query() params) {
    return this.task_historyService.timeline(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.task_historyService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateTask_historyDto: UpdateTask_historyDto) {
    return this.task_historyService.update(+id, updateTask_historyDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.task_historyService.remove(+id);
  }
}
