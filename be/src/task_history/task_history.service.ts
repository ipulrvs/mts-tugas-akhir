import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTask_historyDto } from './dto/create-task_history.dto';
import { UpdateTask_historyDto } from './dto/update-task_history.dto';
import { Task_history } from './entities/task_history.entity';
import { Like } from "typeorm"

@Injectable()
export class Task_historyService {
  constructor(
    @InjectRepository(Task_history)
    private task_historyRepository: Repository<Task_history>,
  ) { }

  async create(createTask_historyDto: CreateTask_historyDto) {
    var inserted = await this.task_historyRepository.insert(createTask_historyDto);
    return {
      ...createTask_historyDto
    }
  }

  async findAll(params): Promise<any> {
    console.log(params, "TELL ME ss")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    where.proyek_id = params.proyek_id
    if(params.sprint_id) where.sprint = {
      id: params.sprint_id
    }
    if(params.tugas_dikerjakan_oleh){
      where.tugas_dikerjakan_oleh = {
        id: params.tugas_dikerjakan_oleh
      }
    }
    if(params.task_id){
      where.task = {
        id: params.task_id
      }
    }

    var order = {}
    order[sortBy] = sort
    const getData = await this.task_historyRepository.findAndCount({
      relations: ['task'],
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  async timeline(params): Promise<any> {
    console.log(params, "TELL ME")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    where.proyek_id = params.proyek_id
    where.timeline = {
      id: params.timeline_id
    }
    if(params.sprint_id) where.sprint = {
      id: params.sprint_id
    }

    var order = {}
    order[sortBy] = sort
    const getData = await this.task_historyRepository.findAndCount({
      relations: ['tugas_dilaporkan_oleh', 'tugas_dikerjakan_oleh', 'tipe_tugas', 'timeline', 'sprint', 'status'],
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  async allData(params): Promise<any> {
    console.log(params, "TELL ME")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    try {
      console.log(typeof where, "WHERE")
      if(params.userId){
        params.where = {
          ...params.where,
          tugas_dikerjakan_oleh: { id: params.userId },
        }
        delete params.userId
        console.log(where, params)
      }
      where = params.where
    } catch(e){}
    try {
      where.proyek_id = params.proyek_id
    } catch(e){}

    try {
      where.task = {
        id: params.task_id
      }
    } catch(e){}
    
    console.log(where, "FINAL WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.task_historyRepository.findAndCount({
      relations: ['tugas_dilaporkan_oleh', 'tugas_dikerjakan_oleh', 'tipe_tugas', 'timeline', 'sprint', 'status'],
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.task_historyRepository.findOne({
      relations: ['tugas_dilaporkan_oleh', 'tugas_dikerjakan_oleh', 'tipe_tugas', 'timeline', 'sprint', 'status'],
      where: {
        id: id,
      },
    });
  }

  update(id: number, updateTask_historyDto: UpdateTask_historyDto) {
    console.log(updateTask_historyDto)
    return this.task_historyRepository.save(updateTask_historyDto);
  }

  remove(id: number) {
    return this.task_historyRepository.delete(id);
  }

  getTotal(){
    return this.task_historyRepository.count()
  }

  getAll(params){
    return this.task_historyRepository.find(params)
  }

}
