import { Task } from 'src/task/entities/task.entity';
import { ManyToOne, JoinColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Task_history {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'longtext' })
    keterangan: string;

    @ManyToOne(() => Task)
    @JoinColumn()
    task: Task

    @Column()
    dibuat_oleh: string

    @CreateDateColumn()
    dibuat_tgl: Date

    @Column()
    diupdate_oleh: string

    @UpdateDateColumn()
    diupdate_tgl: Date
}