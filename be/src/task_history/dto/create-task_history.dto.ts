import { IsEmail, IsNotEmpty } from 'class-validator';
import { Task } from 'src/task/entities/task.entity';


export class CreateTask_historyDto {
    @IsNotEmpty()
    keterangan: string;

    @IsNotEmpty()
    task: Task

    dibuat_oleh: string

    dibuat_tgl: Date

    diupdate_oleh: string

    diupdate_tgl: Date
}
