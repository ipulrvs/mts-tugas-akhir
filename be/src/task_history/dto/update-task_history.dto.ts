import { PartialType } from '@nestjs/mapped-types';
import { CreateTask_historyDto } from './create-task_history.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateTask_historyDto extends PartialType(CreateTask_historyDto) {}
