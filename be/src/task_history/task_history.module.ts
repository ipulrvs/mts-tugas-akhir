import { Module } from '@nestjs/common';
import { Task_historyService } from './task_history.service';
import { Task_historyController } from './task_history.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Task_history } from './entities/task_history.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Task_history])],
  controllers: [Task_historyController],
  providers: [Task_historyService],
  exports: [Task_historyService]
})
export class Task_historyModule {}
