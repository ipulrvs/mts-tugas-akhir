import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ProyekService } from './proyek.service';
import { CreateProyekDto } from './dto/create-proyek.dto';
import { UpdateProyekDto } from './dto/update-proyek.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('proyek')
@Controller('proyek')
export class ProyekController {
  constructor(private readonly proyekService: ProyekService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createProyekDto: CreateProyekDto) {
    return this.proyekService.create(createProyekDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.proyekService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.proyekService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateProyekDto: UpdateProyekDto) {
    return this.proyekService.update(+id, updateProyekDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.proyekService.remove(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/dashboard/admin')
  dashboardAdmin() {
    return this.proyekService.dashboardAdmin();
  }
}
