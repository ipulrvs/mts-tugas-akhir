import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProyekDto } from './dto/create-proyek.dto';
import { UpdateProyekDto } from './dto/update-proyek.dto';
import { Proyek } from './entities/proyek.entity';
import { Like } from "typeorm"
import { TaskService } from 'src/task/task.service';
import { UserService } from 'src/user/user.service';
import { Tipe_tugasService } from 'src/tipe_tugas/tipe_tugas.service';
import { KolomService } from 'src/kolom/kolom.service';
import _ from 'underscore'
import { MemberService } from 'src/member/member.service';
import { Member } from 'src/member/entities/member.entity';
 
@Injectable()
export class ProyekService {
  constructor(
    @InjectRepository(Proyek)
    private proyekRepository: Repository<Proyek>,
    private taskService: TaskService,
    private userService: UserService,
    private tipe_tugasService: Tipe_tugasService,
    private kolomService: KolomService,
    private memberService: MemberService,
  ) { }

  async create(createProyekDto: CreateProyekDto) {
    var inserted = await this.proyekRepository.insert(createProyekDto);
    return {
      ...createProyekDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    var order = {}
    order[sortBy] = sort
    try {
      var userId = params.userId
      if(userId){
        var proyek = []
        var member = await this.memberService.getAll({
          where: {
            member: {
              id: userId
            }
          }
        })
        var memberGetEachProyek = await Promise.all(member[0].map(async (d, index)=> {
          return new Promise(async (resolve, reject)=> {
            var proyek = await this.proyekRepository.findOne({
              where: {
                id: d.proyek_id
              }
            })
            if(proyek){
              resolve(proyek)
            } else {
              reject()
            }
          })
        }))
        console.log(memberGetEachProyek)
        var resultData1 = {
          data: memberGetEachProyek,
          total: memberGetEachProyek.length
        }
        
        return resultData1
      } else {
        const getData = await this.proyekRepository.findAndCount({
          where: where,
          order: order,
          take: limit,
          skip: limit*page,
        });
        var resultData = {
          data: getData[0],
          total: getData[1]
        }
        return resultData
      }
    } catch(e){
      return {
        data: [],
        total: 0,
      }
    }
  }

  findOne(id: number) {
    return this.proyekRepository.findOneBy({ id });
  }

  update(id: number, updateProyekDto: UpdateProyekDto) {
    console.log(updateProyekDto)
    return this.proyekRepository.save(updateProyekDto);
  }

  remove(id: number) {
    return this.proyekRepository.delete(id);
  }

  async dashboardUser(){
      // TODO
  }

  async dashboardAdmin(){
    var data = {
      jumlahProyekBerjalan: 0,
      jumlahProyekSelesai: 0,
      jumlahTask: 0,
      jumlahUser: 0,
      listProyek: [],
      taskPerTipe: {},
      taskPerStatus: {}
    }
    try {
      var getStatus = await this.kolomService.getAll()
      var uniqStatus = _.uniq(getStatus, 'nama')
      console.log(getStatus)
      var getTaskByStatus = await Promise.all(uniqStatus.map(async (d)=> {
        return new Promise(async (resolve, reject)=> {
          try {
            let res = await this.taskService.getAll({
              where: {
                status: {
                  id: d.id
                }
              }
            })
            resolve(res.length)
          } catch(e){
            resolve(0)
          }
        })
      }))
      var getStatusLabel = []
      uniq.map((d)=> { getStatusLabel.push(d.nama) })
      data.taskPerStatus = {
        label: getStatusLabel,
        data: getTaskByStatus
      }
    } catch(e){
      console.log(e)
    }
    try {
      var getTipeTugas = await this.tipe_tugasService.getAll()
      var uniq = _.uniq(getTipeTugas, 'nama')
      var getTaskByTipeTugas = await Promise.all(uniq.map(async (d)=> {
        return new Promise(async (resolve, reject)=> {
          try {
            let res = await this.taskService.getAll({
              where: {
                tipe_tugas: {
                  id: d.id
                }
              }
            })
            resolve(res.length)
          } catch(e){
            resolve(0)
          }
        })
      }))
      var getTipeTugasLabel = []
      uniq.map((d)=> { getTipeTugasLabel.push(d.nama) })
      data.taskPerTipe = {
        label: getTipeTugasLabel,
        data: getTaskByTipeTugas
      }
    } catch(e){
      console.log(e)
    }
    try {
      var jumlahProyekBerjalan = await this.proyekRepository.findAndCount({
        where: { "status": "BERJALAN" }
      })
      var jumlahProyekSelesai = await this.proyekRepository.findAndCount({
        where: { "status": "SELESAI" }
      })
      var listProyek = await this.proyekRepository.find({
        take: 5,
        where: {},
        order: {
          'id': "DESC"
        }
      })
      var jumlahTask = await this.taskService.getTotal()
      var jumlahUser = await this.userService.getTotal()
      data.jumlahProyekBerjalan = jumlahProyekBerjalan[1]
      data.jumlahProyekSelesai = jumlahProyekSelesai[1]
      data.jumlahTask = jumlahTask
      data.jumlahUser = jumlahUser
      data.listProyek = listProyek
      return data
    } catch(e){
      console.log(e)
      return data
    }
  }
}
