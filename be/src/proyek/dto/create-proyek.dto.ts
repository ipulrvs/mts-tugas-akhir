import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateProyekDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    @IsNotEmpty()
    status: string

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number
}
