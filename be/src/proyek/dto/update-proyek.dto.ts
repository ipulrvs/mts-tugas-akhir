import { PartialType } from '@nestjs/mapped-types';
import { CreateProyekDto } from './create-proyek.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateProyekDto extends PartialType(CreateProyekDto) {}
