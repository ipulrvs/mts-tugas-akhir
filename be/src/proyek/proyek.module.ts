import { Module } from '@nestjs/common';
import { ProyekService } from './proyek.service';
import { ProyekController } from './proyek.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Proyek } from './entities/proyek.entity';

import { TaskModule } from 'src/task/task.module';
import { UserModule } from 'src/user/user.module';
import { KolomModule } from 'src/kolom/kolom.module';
import { Tipe_tugasModule } from 'src/tipe_tugas/tipe_tugas.module';
import { MemberModule } from 'src/member/member.module';

@Module({
  imports: [TaskModule, UserModule, Tipe_tugasModule, KolomModule, MemberModule, TypeOrmModule.forFeature([Proyek])],
  controllers: [ProyekController],
  providers: [ProyekService]
})
export class ProyekModule {}
