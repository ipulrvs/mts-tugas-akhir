import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Proyek {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nama: string;

    @Column()
    keterangan: string;

    @Column()
    status: string;

    @Column()
    dibuat_oleh: string

    @CreateDateColumn()
    dibuat_tgl: Date

    @Column()
    diupdate_oleh: string

    @UpdateDateColumn()
    diupdate_tgl: Date

    @Column()
    user_id: number
}
