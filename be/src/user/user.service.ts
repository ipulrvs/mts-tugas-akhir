import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { Like } from 'typeorm';
import * as bcrypt from 'bcrypt';

console.log(bcrypt.hashSync('admin', 10), 'PASSWORD admin');

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const hashedPassword = await bcrypt.hash(createUserDto.password, 10);

    createUserDto.password = hashedPassword;

    let inserted = await this.userRepository.insert(createUserDto);
    return {
      ...createUserDto,
    };
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 20;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      try {
        where = JSON.parse(where);
        if (where.username) where.username = Like(`%${where.username}%`);
      } catch (e) {}
    }
    var order = {};
    order[sortBy] = sort;
    const getData = await this.userRepository.findAndCount({
      relations: ['role', 'jabatan', 'departemen', 'jadwal'],
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  findOne(id: number) {
    return this.userRepository.findOne({
      relations: ['role', 'jabatan', 'departemen', 'jadwal'],
      where: {
        id: id,
      },
    });
  }

  findOneByUsername(username: string) {
    return this.userRepository.findOneBy({ username });
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    console.log(updateUserDto);
    delete updateUserDto.password;
    return this.userRepository.save(updateUserDto);
  }

  remove(id: number) {
    return this.userRepository.delete(id);
  }

  getTotal(){
    return this.userRepository.count()
  }

}
