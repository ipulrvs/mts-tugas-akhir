import { IsEmail, IsNotEmpty } from 'class-validator';
import { Role } from '../../role/entities/role.entity'

export class CreateUserDto {
    @IsNotEmpty()
    username: string

    @IsNotEmpty()
    password: string

    nama: string

    foto: string

    role: Role

    jabatan_id: number

    departemen_id: number

    jadwal_id: number

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number
}
