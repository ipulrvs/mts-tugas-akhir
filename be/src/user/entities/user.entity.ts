import { JoinColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, OneToOne, ManyToOne } from 'typeorm';
import { Role } from '../../role/entities/role.entity'
import { Jabatan } from 'src/jabatan/entities/jabatan.entity';
import { Departemen } from 'src/departemen/entities/departemen.entity';
import { Jadwal } from 'src/jadwal/entities/jadwal.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @Column()
    nama: string;

    @ManyToOne(() => Role, {
        eager: true,
    })
    @JoinColumn()
    role: Role

    @ManyToOne(() => Jabatan)
    @JoinColumn()
    jabatan: Jabatan

    @ManyToOne(() => Departemen)
    @JoinColumn()
    departemen: Departemen

    @ManyToOne(() => Jadwal)
    @JoinColumn()
    jadwal: Jadwal

    @Column()
    dibuat_oleh: string

    @CreateDateColumn()
    dibuat_tgl: Date

    @Column()
    diupdate_oleh: string

    @UpdateDateColumn()
    diupdate_tgl: Date

    @Column()
    user_id: number

    @Column("longtext") 
    foto: string;
}
