import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Role_permissionService } from './role_permission.service';
import { CreateRole_permissionDto } from './dto/create-role_permission.dto';
import { UpdateRole_permissionDto } from './dto/update-role_permission.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('role_permission')
@Controller('role_permission')
export class Role_permissionController {
  constructor(private readonly role_permissionService: Role_permissionService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createRole_permissionDto: CreateRole_permissionDto) {
    return this.role_permissionService.create(createRole_permissionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.role_permissionService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.role_permissionService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateRole_permissionDto: UpdateRole_permissionDto) {
    return this.role_permissionService.update(+id, updateRole_permissionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.role_permissionService.remove(+id);
  }
}
