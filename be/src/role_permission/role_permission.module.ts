import { Module } from '@nestjs/common';
import { Role_permissionService } from './role_permission.service';
import { Role_permissionController } from './role_permission.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Role_permission } from './entities/role_permission.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Role_permission])],
  controllers: [Role_permissionController],
  providers: [Role_permissionService]
})
export class Role_permissionModule {}
