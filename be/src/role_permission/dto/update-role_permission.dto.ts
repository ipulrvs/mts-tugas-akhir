import { PartialType } from '@nestjs/mapped-types';
import { CreateRole_permissionDto } from './create-role_permission.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateRole_permissionDto extends PartialType(CreateRole_permissionDto) {}
