import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateRole_permissionDto {
    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    @IsNotEmpty()
    user_id: number

    @IsNotEmpty()
    menu_id: number

    @IsNotEmpty()
    permission_id: number

    @IsNotEmpty()
    check: boolean
}
