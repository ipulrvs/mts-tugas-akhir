import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRole_permissionDto } from './dto/create-role_permission.dto';
import { UpdateRole_permissionDto } from './dto/update-role_permission.dto';
import { Role_permission } from './entities/role_permission.entity';

@Injectable()
export class Role_permissionService {
  constructor(
    @InjectRepository(Role_permission)
    private role_permissionRepository: Repository<Role_permission>,
  ) { }

  async create(createRole_permissionDto: CreateRole_permissionDto) {
    var inserted = await this.role_permissionRepository.insert(createRole_permissionDto);
    return {
      ...createRole_permissionDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      where = JSON.parse(where)
    }
    console.log(typeof where, "WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.role_permissionRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.role_permissionRepository.findOneBy({ id });
  }

  update(id: number, updateRole_permissionDto: UpdateRole_permissionDto) {
    console.log(updateRole_permissionDto)
    return this.role_permissionRepository.save(updateRole_permissionDto);
  }

  remove(id: number) {
    return this.role_permissionRepository.delete(id);
  }
}
