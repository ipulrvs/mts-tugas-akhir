import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Jadwal_perorangService } from './jadwal_perorang.service';
import { CreateJadwal_perorangDto } from './dto/create-jadwal_perorang.dto';
import { UpdateJadwal_perorangDto } from './dto/update-jadwal_perorang.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('jadwal_perorang')
@Controller('jadwal_perorang')
export class Jadwal_perorangController {
  constructor(private readonly jadwal_perorangService: Jadwal_perorangService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createJadwal_perorangDto: CreateJadwal_perorangDto) {
    return this.jadwal_perorangService.create(createJadwal_perorangDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.jadwal_perorangService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.jadwal_perorangService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateJadwal_perorangDto: UpdateJadwal_perorangDto) {
    return this.jadwal_perorangService.update(+id, updateJadwal_perorangDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.jadwal_perorangService.remove(+id);
  }
}
