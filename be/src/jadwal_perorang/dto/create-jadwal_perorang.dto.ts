import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateJadwal_perorangDto {
    @IsNotEmpty()
    user_id: number

    @IsNotEmpty()
    jadwal_id: number
}
