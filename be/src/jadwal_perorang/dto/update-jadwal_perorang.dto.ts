import { PartialType } from '@nestjs/mapped-types';
import { CreateJadwal_perorangDto } from './create-jadwal_perorang.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateJadwal_perorangDto extends PartialType(CreateJadwal_perorangDto) {}
