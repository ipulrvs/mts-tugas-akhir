import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateJadwal_perorangDto } from './dto/create-jadwal_perorang.dto';
import { UpdateJadwal_perorangDto } from './dto/update-jadwal_perorang.dto';
import { Jadwal_perorang } from './entities/jadwal_perorang.entity';

@Injectable()
export class Jadwal_perorangService {
  constructor(
    @InjectRepository(Jadwal_perorang)
    private jadwal_perorangRepository: Repository<Jadwal_perorang>,
  ) { }

  async create(createJadwal_perorangDto: CreateJadwal_perorangDto) {
    var inserted = await this.jadwal_perorangRepository.insert(createJadwal_perorangDto);
    return {
      ...createJadwal_perorangDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      where = JSON.parse(where)
    }
    console.log(typeof where, "WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.jadwal_perorangRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.jadwal_perorangRepository.findOneBy({ id });
  }

  update(id: number, updateJadwal_perorangDto: UpdateJadwal_perorangDto) {
    console.log(updateJadwal_perorangDto)
    return this.jadwal_perorangRepository.save(updateJadwal_perorangDto);
  }

  remove(id: number) {
    return this.jadwal_perorangRepository.delete(id);
  }
}
