import { Module } from '@nestjs/common';
import { Jadwal_perorangService } from './jadwal_perorang.service';
import { Jadwal_perorangController } from './jadwal_perorang.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Jadwal_perorang } from './entities/jadwal_perorang.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Jadwal_perorang])],
  controllers: [Jadwal_perorangController],
  providers: [Jadwal_perorangService]
})
export class Jadwal_perorangModule {}
