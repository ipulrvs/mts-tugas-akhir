import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDepartemenDto } from './dto/create-departemen.dto';
import { UpdateDepartemenDto } from './dto/update-departemen.dto';
import { Departemen } from './entities/departemen.entity';
import { Like } from "typeorm"

@Injectable()
export class DepartemenService {
  constructor(
    @InjectRepository(Departemen)
    private departemenRepository: Repository<Departemen>,
  ) { }

  async create(createDepartemenDto: CreateDepartemenDto) {
    var inserted = await this.departemenRepository.insert(createDepartemenDto);
    return {
      ...createDepartemenDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    var order = {}
    order[sortBy] = sort
    const getData = await this.departemenRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.departemenRepository.findOneBy({ id });
  }

  update(id: number, updateDepartemenDto: UpdateDepartemenDto) {
    console.log(updateDepartemenDto)
    return this.departemenRepository.save(updateDepartemenDto);
  }

  remove(id: number) {
    return this.departemenRepository.delete(id);
  }
}
