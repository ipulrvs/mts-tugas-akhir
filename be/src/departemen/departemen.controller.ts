import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DepartemenService } from './departemen.service';
import { CreateDepartemenDto } from './dto/create-departemen.dto';
import { UpdateDepartemenDto } from './dto/update-departemen.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('departemen')
@Controller('departemen')
export class DepartemenController {
  constructor(private readonly departemenService: DepartemenService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createDepartemenDto: CreateDepartemenDto) {
    return this.departemenService.create(createDepartemenDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.departemenService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.departemenService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateDepartemenDto: UpdateDepartemenDto) {
    return this.departemenService.update(+id, updateDepartemenDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.departemenService.remove(+id);
  }
}
