import { Module } from '@nestjs/common';
import { DepartemenService } from './departemen.service';
import { DepartemenController } from './departemen.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Departemen } from './entities/departemen.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Departemen])],
  controllers: [DepartemenController],
  providers: [DepartemenService]
})
export class DepartemenModule {}
