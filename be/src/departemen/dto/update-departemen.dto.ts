import { PartialType } from '@nestjs/mapped-types';
import { CreateDepartemenDto } from './create-departemen.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateDepartemenDto extends PartialType(CreateDepartemenDto) {}
