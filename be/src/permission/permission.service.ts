import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { Permission } from './entities/permission.entity';

@Injectable()
export class PermissionService {
  constructor(
    @InjectRepository(Permission)
    private permissionRepository: Repository<Permission>,
  ) { }

  async create(createPermissionDto: CreatePermissionDto) {
    var inserted = await this.permissionRepository.insert(createPermissionDto);
    return {
      ...createPermissionDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      where = JSON.parse(where)
    }
    console.log(typeof where, "WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.permissionRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.permissionRepository.findOneBy({ id });
  }

  update(id: number, updatePermissionDto: UpdatePermissionDto) {
    console.log(updatePermissionDto)
    return this.permissionRepository.save(updatePermissionDto);
  }

  remove(id: number) {
    return this.permissionRepository.delete(id);
  }
}
