import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { Like } from "typeorm"

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private memberRepository: Repository<Member>,
  ) { }

  async create(createMemberDto: CreateMemberDto) {
    var inserted = await this.memberRepository.insert(createMemberDto);
    return {
      ...createMemberDto
    }
  }

  async findAll(params): Promise<any> {
    console.log(params, "PARAMS ")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    console.log(where, "WHERE")
    where.proyek_id = params.proyek_id
    var order = {}
    order[sortBy] = sort
    const getData = await this.memberRepository.findAndCount({
      relations: ['member'],
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.memberRepository.findOne({ 
      relations: ['member'],
      where: {
        id:id,
      }
    });
  }

  update(id: number, updateMemberDto: UpdateMemberDto) {
    console.log(updateMemberDto)
    return this.memberRepository.save(updateMemberDto);
  }

  remove(id: number) {
    return this.memberRepository.delete(id);
  }

  getAll(params){
    return this.memberRepository.findAndCount(params)
  }
}
