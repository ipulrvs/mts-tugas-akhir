import { IsEmail, isNotEmpty, IsNotEmpty } from 'class-validator';
import { User } from 'src/user/entities/user.entity';

export class CreateMemberDto {
    nama: string

    keterangan: string

    @IsNotEmpty()
    member: User

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number

    proyek_id: number
}
