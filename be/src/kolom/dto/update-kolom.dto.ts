import { PartialType } from '@nestjs/mapped-types';
import { CreateKolomDto } from './create-kolom.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateKolomDto extends PartialType(CreateKolomDto) {}
