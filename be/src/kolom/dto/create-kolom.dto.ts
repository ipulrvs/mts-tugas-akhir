import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateKolomDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    task_selesai: boolean

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number

    proyek_id: number
}
