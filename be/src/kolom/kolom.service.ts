import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateKolomDto } from './dto/create-kolom.dto';
import { UpdateKolomDto } from './dto/update-kolom.dto';
import { Kolom } from './entities/kolom.entity';
import { Like } from "typeorm"

@Injectable()
export class KolomService {
  constructor(
    @InjectRepository(Kolom)
    private kolomRepository: Repository<Kolom>,
  ) { }

  async create(createKolomDto: CreateKolomDto) {
    var inserted = await this.kolomRepository.insert(createKolomDto);
    return {
      ...createKolomDto
    }
  }

  async findAll(params): Promise<any> {
    console.log(params, "PARAMS ")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}

    if(params.proyek_id) where.proyek_id = params.proyek_id
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    console.log(where, "WHERE")
    where.proyek_id = params.proyek_id
    var order = {}
    order[sortBy] = sort
    const getData = await this.kolomRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.kolomRepository.findOneBy({ id });
  }

  update(id: number, updateKolomDto: UpdateKolomDto) {
    console.log(updateKolomDto)
    return this.kolomRepository.save(updateKolomDto);
  }

  remove(id: number) {
    return this.kolomRepository.delete(id);
  }

  getAll(){
    return this.kolomRepository.find()
  }
}
