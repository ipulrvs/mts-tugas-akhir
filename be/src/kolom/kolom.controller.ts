import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { KolomService } from './kolom.service';
import { CreateKolomDto } from './dto/create-kolom.dto';
import { UpdateKolomDto } from './dto/update-kolom.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('kolom')
@Controller('kolom')
export class KolomController {
  constructor(private readonly kolomService: KolomService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createKolomDto: CreateKolomDto) {
    return this.kolomService.create(createKolomDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    return this.kolomService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.kolomService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateKolomDto: UpdateKolomDto) {
    return this.kolomService.update(+id, updateKolomDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.kolomService.remove(+id);
  }
}
