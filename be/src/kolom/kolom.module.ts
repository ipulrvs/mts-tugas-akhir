import { Module } from '@nestjs/common';
import { KolomService } from './kolom.service';
import { KolomController } from './kolom.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Kolom } from './entities/kolom.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Kolom])],
  controllers: [KolomController],
  providers: [KolomService],
  exports: [KolomService]
})
export class KolomModule {}
