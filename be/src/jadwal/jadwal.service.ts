import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateJadwalDto } from './dto/create-jadwal.dto';
import { UpdateJadwalDto } from './dto/update-jadwal.dto';
import { Jadwal } from './entities/jadwal.entity';

@Injectable()
export class JadwalService {
  constructor(
    @InjectRepository(Jadwal)
    private jadwalRepository: Repository<Jadwal>,
  ) { }

  async create(createJadwalDto: CreateJadwalDto) {
    var inserted = await this.jadwalRepository.insert(createJadwalDto);
    return {
      ...createJadwalDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      where = JSON.parse(where)
    }
    console.log(typeof where, "WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.jadwalRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.jadwalRepository.findOneBy({ id });
  }

  update(id: number, updateJadwalDto: UpdateJadwalDto) {
    console.log(updateJadwalDto)
    return this.jadwalRepository.save(updateJadwalDto);
  }

  remove(id: number) {
    return this.jadwalRepository.delete(id);
  }
}
