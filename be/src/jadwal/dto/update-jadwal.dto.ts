import { PartialType } from '@nestjs/mapped-types';
import { CreateJadwalDto } from './create-jadwal.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateJadwalDto extends PartialType(CreateJadwalDto) {}
