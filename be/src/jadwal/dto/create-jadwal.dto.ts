import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateJadwalDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    @IsNotEmpty()
    jam_masuk: string

    @IsNotEmpty()
    jam_keluar: string

    toleransi_jam_masuk: string

    toleransi_jam_keluar: string
}
