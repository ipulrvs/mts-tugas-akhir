import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMenuDto } from './dto/create-menu.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Menu } from './entities/menu.entity';

@Injectable()
export class MenuService {
  constructor(
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
  ) { }

  async create(createMenuDto: CreateMenuDto) {
    var inserted = await this.menuRepository.insert(createMenuDto);
    return {
      ...createMenuDto
    }
  }

  async findAll(params): Promise<any> {
    const limit = params.limit || 10
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      where = JSON.parse(where)
    }
    console.log(typeof where, "WHERE")
    var order = {}
    order[sortBy] = sort
    const getData = await this.menuRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.menuRepository.findOneBy({ id });
  }

  update(id: number, updateMenuDto: UpdateMenuDto) {
    console.log(updateMenuDto)
    return this.menuRepository.save(updateMenuDto);
  }

  remove(id: number) {
    return this.menuRepository.delete(id);
  }
}
