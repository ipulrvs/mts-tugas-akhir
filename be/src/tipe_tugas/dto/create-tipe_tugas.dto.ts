import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateTipe_tugasDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number

    proyek_id: number
}
