import { PartialType } from '@nestjs/mapped-types';
import { CreateTipe_tugasDto } from './create-tipe_tugas.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateTipe_tugasDto extends PartialType(CreateTipe_tugasDto) {}
