import { Module } from '@nestjs/common';
import { Tipe_tugasService } from './tipe_tugas.service';
import { Tipe_tugasController } from './tipe_tugas.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Tipe_tugas } from './entities/tipe_tugas.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Tipe_tugas])],
  controllers: [Tipe_tugasController],
  providers: [Tipe_tugasService],
  exports: [Tipe_tugasService]
})
export class Tipe_tugasModule {}
