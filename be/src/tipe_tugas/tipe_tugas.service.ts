import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTipe_tugasDto } from './dto/create-tipe_tugas.dto';
import { UpdateTipe_tugasDto } from './dto/update-tipe_tugas.dto';
import { Tipe_tugas } from './entities/tipe_tugas.entity';
import { Like } from "typeorm"

@Injectable()
export class Tipe_tugasService {
  constructor(
    @InjectRepository(Tipe_tugas)
    private tipe_tugasRepository: Repository<Tipe_tugas>,
  ) { }

  async create(createTipe_tugasDto: CreateTipe_tugasDto) {
    var inserted = await this.tipe_tugasRepository.insert(createTipe_tugasDto);
    return {
      ...createTipe_tugasDto
    }
  }

  async findAll(params): Promise<any> {
    console.log(params, "TELL ME")
    const limit = params.limit || 20
    const keyword = params.keyword || 0
    const page = params.page || 0
    const sortBy = params.sortBy || 'id'
    const sort = params.sort || 'DESC'
    var where = params.where || {}
    if(typeof where == 'string'){
      try {
        where = JSON.parse(where)
        if(where.nama) where.nama = Like(`%${where.nama}%`)
        if(where.keterangan) where.keterangan = Like(`%${where.keterangan}%`)
      } catch(e){}
    }
    where.proyek_id = params.proyek_id
    var order = {}
    order[sortBy] = sort
    const getData = await this.tipe_tugasRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit*page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1]
    }
    return resultData
  }

  findOne(id: number) {
    return this.tipe_tugasRepository.findOneBy({ id });
  }

  update(id: number, updateTipe_tugasDto: UpdateTipe_tugasDto) {
    console.log(updateTipe_tugasDto)
    return this.tipe_tugasRepository.save(updateTipe_tugasDto);
  }

  remove(id: number) {
    return this.tipe_tugasRepository.delete(id);
  }

  getAll(){
    return this.tipe_tugasRepository.find()
  }
}
