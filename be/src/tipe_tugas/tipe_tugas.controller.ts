import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Tipe_tugasService } from './tipe_tugas.service';
import { CreateTipe_tugasDto } from './dto/create-tipe_tugas.dto';
import { UpdateTipe_tugasDto } from './dto/update-tipe_tugas.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('tipe_tugas')
@Controller('tipe_tugas')
export class Tipe_tugasController {
  constructor(private readonly tipe_tugasService: Tipe_tugasService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createTipe_tugasDto: CreateTipe_tugasDto) {
    return this.tipe_tugasService.create(createTipe_tugasDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    console.log(params)
    return this.tipe_tugasService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.tipe_tugasService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateTipe_tugasDto: UpdateTipe_tugasDto) {
    return this.tipe_tugasService.update(+id, updateTipe_tugasDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.tipe_tugasService.remove(+id);
  }
}
