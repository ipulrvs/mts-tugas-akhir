import { IsEmail, IsNotEmpty } from 'class-validator';
import { Kolom } from 'src/kolom/entities/kolom.entity';
import { Sprint } from 'src/sprint/entities/sprint.entity';
import { Timeline } from 'src/timeline/entities/timeline.entity';
import { Tipe_tugas } from 'src/tipe_tugas/entities/tipe_tugas.entity';
import { User } from 'src/users/users.service';

export class CreateTaskDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    prioritas: string

    point: number

    estimasi_awal: number

    estimasi_dikerjakan: number

    @IsNotEmpty()
    status: Kolom

    tipe_tugas: Tipe_tugas

    sprint: Sprint

    timeline: Timeline

    tugas_dilaporkan_oleh: User

    tugas_dikerjakan_oleh: User

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number

    proyek_id: number
}
