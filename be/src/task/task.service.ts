import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';
import { Like } from "typeorm"
import * as xlsx from 'xlsx'
import * as moment from 'moment';
import { Proyek } from 'src/proyek/entities/proyek.entity';
import { ProyekService } from 'src/proyek/proyek.service';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private taskRepository: Repository<Task>,
  ) {}

  async create(createTaskDto: CreateTaskDto) {
    var inserted = await this.taskRepository.insert(createTaskDto);
    return {
      ...createTaskDto,
    };
  }

  async findAll(params): Promise<any> {
    console.log(params, 'TELL ME');
    const limit = params.limit || 20;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      try {
        where = JSON.parse(where);
        if (where.nama) where.nama = Like(`%${where.nama}%`);
        if (where.keterangan) where.keterangan = Like(`%${where.keterangan}%`);
      } catch (e) {}
    }
    where.proyek_id = params.proyek_id;
    if (params.sprint_id)
      where.sprint = {
        id: params.sprint_id,
      };
    if (params.tugas_dikerjakan_oleh) {
      where.tugas_dikerjakan_oleh = {
        id: params.tugas_dikerjakan_oleh,
      };
    }

    var order = {};
    order[sortBy] = sort;
    const getData = await this.taskRepository.findAndCount({
      relations: [
        'tugas_dilaporkan_oleh',
        'tugas_dikerjakan_oleh',
        'tipe_tugas',
        'timeline',
        'sprint',
        'status',
      ],
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  async timeline(params): Promise<any> {
    console.log(params, 'TELL ME');
    const limit = params.limit || 20;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      try {
        where = JSON.parse(where);
        if (where.nama) where.nama = Like(`%${where.nama}%`);
        if (where.keterangan) where.keterangan = Like(`%${where.keterangan}%`);
      } catch (e) {}
    }
    where.proyek_id = params.proyek_id;
    where.timeline = {
      id: params.timeline_id,
    };
    if (params.sprint_id)
      where.sprint = {
        id: params.sprint_id,
      };

    var order = {};
    order[sortBy] = sort;
    const getData = await this.taskRepository.findAndCount({
      relations: [
        'tugas_dilaporkan_oleh',
        'tugas_dikerjakan_oleh',
        'tipe_tugas',
        'timeline',
        'sprint',
        'status',
      ],
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  async allData(params): Promise<any> {
    console.log(params, 'TELL ME');
    const limit = params.limit || 20;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      try {
        where = JSON.parse(where);
        if (where.nama) where.nama = Like(`%${where.nama}%`);
        if (where.keterangan) where.keterangan = Like(`%${where.keterangan}%`);
      } catch (e) {}
    }
    try {
      console.log(typeof where, 'WHERE');
      if (params.userId) {
        params.where = {
          ...params.where,
          tugas_dikerjakan_oleh: { id: params.userId },
        };
        delete params.userId;
        console.log(where, params);
      }
      where = params.where;
    } catch (e) {}
    try {
      where.proyek_id = params.proyek_id;
    } catch (e) {}
    console.log(where, 'FINAL WHERE');
    var order = {};
    order[sortBy] = sort;
    const getData = await this.taskRepository.findAndCount({
      relations: [
        'tugas_dilaporkan_oleh',
        'tugas_dikerjakan_oleh',
        'tipe_tugas',
        'timeline',
        'sprint',
        'status',
      ],
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  findOne(id: number) {
    return this.taskRepository.findOne({
      relations: [
        'tugas_dilaporkan_oleh',
        'tugas_dikerjakan_oleh',
        'tipe_tugas',
        'timeline',
        'sprint',
        'status',
      ],
      where: {
        id: id,
      },
    });
  }

  update(id: number, updateTaskDto: UpdateTaskDto) {
    console.log(updateTaskDto);
    return this.taskRepository.save(updateTaskDto);
  }

  remove(id: number) {
    return this.taskRepository.delete(id);
  }

  getTotal() {
    return this.taskRepository.count();
  }

  getAll(params) {
    return this.taskRepository.find(params);
  }

  async report() {
    const getData = await this.taskRepository.find({
      relations: [
        'tugas_dilaporkan_oleh',
        'tugas_dikerjakan_oleh',
        'tipe_tugas',
        'timeline',
        'sprint',
        'status',
      ],
    });
    let workbook = xlsx.utils.book_new();
    let worksheet = xlsx.utils.json_to_sheet(
      getData.map((d, i) => ({
        NO: i + 1,
        'TIPE TUGAS': d.tipe_tugas.nama,
        TUGAS: d.nama,
        STATUS: d.status.nama,
        PRIORITAS: d.prioritas,
        'DIKERJAKAN OLEH': d.tugas_dikerjakan_oleh.nama,
        'DILAPORKAN OLEH': d.tugas_dilaporkan_oleh.nama,
        'ID PROYEK': d.proyek_id,
        'TIMELINE': d.timeline.nama,
        'TIMELINE DIMULAI': d.timeline.timeline_dimulai,
        'TIMELINE BERAKHIR': d.timeline.timeline_berakhir,
        'SPRINT': d.sprint.nama,
        'SPRINT DIMULAI': d.sprint.sprint_dimulai,
        'SPRINT BERAKHIR': d.timeline.timeline_berakhir,
        'STATUS SPRINT': d.timeline.timeline_berakhir,
        'DIBUAT TANGGAL': moment(new Date(d.dibuat_tgl)).format('DD-MM-YYYY hh:mm:ss'),
        'DIUPDATE TANGGAL': moment(new Date(d.diupdate_tgl)).format('DD-MM-YYYY hh:mm:ss'),
      })),
    );
    xlsx.utils.book_append_sheet(workbook, worksheet, 'TASK REPORT');
    return xlsx.write(workbook, { type: 'buffer' });
  }
}
