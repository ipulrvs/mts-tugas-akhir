import { Kolom } from 'src/kolom/entities/kolom.entity';
import { Sprint } from 'src/sprint/entities/sprint.entity';
import { Timeline } from 'src/timeline/entities/timeline.entity';
import { Tipe_tugas } from 'src/tipe_tugas/entities/tipe_tugas.entity';
import { User } from 'src/user/entities/user.entity';
import { ManyToOne, JoinColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Task {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nama: string;

    @Column({ type: 'longtext' })
    keterangan: string;

    @Column()
    prioritas: string;

    @Column()
    point: number;

    @Column()
    estimasi_awal: number;

    @Column()
    estimasi_dikerjakan: number;

    @ManyToOne(() => Kolom, {
        eager: true,
    })
    @JoinColumn()
    status: Kolom

    @ManyToOne(() => Tipe_tugas)
    @JoinColumn()
    tipe_tugas: Tipe_tugas

    @ManyToOne(() => Sprint)
    @JoinColumn()
    sprint: Sprint

    @ManyToOne(() => Timeline, (timeline) => timeline.tasks)
    @JoinColumn()
    timeline: Timeline

    @ManyToOne(() => User)
    @JoinColumn()
    tugas_dilaporkan_oleh: User

    @ManyToOne(() => User)
    @JoinColumn()
    tugas_dikerjakan_oleh: User

    @Column()
    dibuat_oleh: string

    @CreateDateColumn()
    dibuat_tgl: Date

    @Column()
    diupdate_oleh: string

    @UpdateDateColumn()
    diupdate_tgl: Date

    @Column()
    user_id: number

    @Column()
    proyek_id: number
}