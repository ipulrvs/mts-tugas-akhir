import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('task')
@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createTaskDto: CreateTaskDto) {
    return this.taskService.create(createTaskDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    console.log(params)
    return this.taskService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  allData(@Query() params) {
    return this.taskService.allData(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/timeline')
  timeline(@Query() params) {
    return this.taskService.timeline(params);
  }

  @Get('/report')
  async report(@Res() res) {
    var buffer = await this.taskService.report();
    res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    res.set('Content-Disposition', 'attachment; filename="task-report.xlsx"')
    res.send(buffer);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.taskService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto) {
    return this.taskService.update(+id, updateTaskDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.taskService.remove(+id);
  }
}
