import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSprintDto } from './dto/create-sprint.dto';
import { UpdateSprintDto } from './dto/update-sprint.dto';
import { Sprint } from './entities/sprint.entity';
import { Like } from 'typeorm';

@Injectable()
export class SprintService {
  constructor(
    @InjectRepository(Sprint)
    private sprintRepository: Repository<Sprint>,
  ) {}

  async create(createSprintDto: CreateSprintDto) {
    var inserted = await this.sprintRepository.insert(createSprintDto);
    return {
      ...createSprintDto,
    };
  }

  async findAll(params): Promise<any> {
    console.log(params, 'TELL ME');
    const limit = params.limit || 20;
    const keyword = params.keyword || 0;
    const page = params.page || 0;
    const sortBy = params.sortBy || 'id';
    const sort = params.sort || 'DESC';
    var where = params.where || {};
    if (typeof where == 'string') {
      try {
        where = JSON.parse(where);
        if (where.nama) where.nama = Like(`%${where.nama}%`);
        if (where.keterangan) where.keterangan = Like(`%${where.keterangan}%`);
      } catch (e) {}
    }
    where.proyek_id = params.proyek_id;
    if (params.status) where.status = params.status;
    var order = {};
    order[sortBy] = sort;
    const getData = await this.sprintRepository.findAndCount({
      where: where,
      order: order,
      take: limit,
      skip: limit * page,
    });
    var resultData = {
      data: getData[0],
      total: getData[1],
    };
    return resultData;
  }

  findOne(id: number) {
    return this.sprintRepository.findOneBy({ id });
  }

  update(id: number, updateSprintDto: UpdateSprintDto) {
    console.log(updateSprintDto);
    return this.sprintRepository.save(updateSprintDto);
  }

  remove(id: number) {
    return this.sprintRepository.delete(id);
  }

  async startSprint(id: number) {
    var d = await this.sprintRepository.findOneBy({ id });
    var check = await this.sprintRepository.find({
      where: {
        proyek_id: d.proyek_id,
        status: 'RUNNING',
      },
    });

    if (check.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Terdapat sprint yang sudah berjalan',
        },
        HttpStatus.FORBIDDEN,
      );
    } else {
      await this.sprintRepository.save({ ...d, status: 'RUNNING' });
    }

    return d;
  }

  async finishSprint(id: number) {
    var d = await this.sprintRepository.findOneBy({ id });

    await this.sprintRepository.save({ ...d, status: 'FINISH' });

    return d;
  }
}
