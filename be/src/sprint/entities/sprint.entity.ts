import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Sprint {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nama: string;

    @Column()
    keterangan: string;

    @Column({type: 'date'})
    sprint_dimulai: Date

    @Column({type: 'date'})
    sprint_berakhir: Date

    @Column()
    status: string;

    @Column()
    dibuat_oleh: string

    @CreateDateColumn()
    dibuat_tgl: Date

    @Column()
    diupdate_oleh: string

    @UpdateDateColumn()
    diupdate_tgl: Date

    @Column()
    user_id: number

    @Column()
    proyek_id: number
}

