import { PartialType } from '@nestjs/mapped-types';
import { CreateSprintDto } from './create-sprint.dto';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateSprintDto extends PartialType(CreateSprintDto) {}
