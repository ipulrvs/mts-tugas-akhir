import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateSprintDto {
    @IsNotEmpty()
    nama: string

    @IsNotEmpty()
    keterangan: string

    @IsNotEmpty()
    sprint_dimulai: Date

    @IsNotEmpty()
    sprint_berakhir: Date

    @IsNotEmpty()
    status: string

    dibuat_oleh: string

    dibuat_tgl: string

    diupdate_oleh: string

    diupdate_tgl: string

    user_id: number

    proyek_id: number
}
