import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SprintService } from './sprint.service';
import { CreateSprintDto } from './dto/create-sprint.dto';
import { UpdateSprintDto } from './dto/update-sprint.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';

@ApiTags('sprint')
@Controller('sprint')
export class SprintController {
  constructor(private readonly sprintService: SprintService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createSprintDto: CreateSprintDto) {
    return this.sprintService.create(createSprintDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Query() params) {
    console.log(params)
    return this.sprintService.findAll(params);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sprintService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateSprintDto: UpdateSprintDto) {
    return this.sprintService.update(+id, updateSprintDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sprintService.remove(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/start/:id')
  startSprint(@Param('id') id: string) {
    return this.sprintService.startSprint(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/finish/:id')
  finishSprint(@Param('id') id: string) {
    return this.sprintService.finishSprint(+id);
  }
}
